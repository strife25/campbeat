class RegistrationsController < ApplicationController
  def show
    @camp = Camp.find_by_uri(params[:camp_id])
  end

  def create

    @camp = Camp.find_by_uri(params[:camp_id])
    
    camper = Camper.new(params[:camper])
    programId = params[:programs][:id]
    sessionId = params[:sessions][:id]

    # Validate that the request's parameteres are acceptable
    unless @camp.programs.exists?(programId)
      self.render_400_with_msg("The program chosen is not provided by the chosen camp.")
      return
    end

    unless @camp.sessions.exists?(sessionId)
      self.render_400_with_msg("The session chosen is not provided by the chosen camp.")
      return
    end

    unless Program.find(programId).is_valid_age(camper.age)
      self.render_400_with_msg("The age of the camper does not fall within the program's age group.")
      return
    end

    pop = ProgramPopulation.where("program_id = ? AND session_id = ?", programId, sessionId).limit(1)[0]

    if pop.nil? or !pop.is_open
      self.render_400_with_msg("There are no vacancies in the chosen program session.")
      return
    end

    # Once the request is validated, save the data.
    if camper.save
      guardian = Contact.create(params[:contact])
      address = Address.create(params[:address])
      
      camper.guardians << guardian
      camper.address = address
      
      @camp.campers << camper
      pop.increment!(:current_population)

      camper.camper_attendances.create(
        :camper_id => camper.id,
        :program_id => programId,
        :session_id => sessionId
      )
    end

  end

end
