class ApplicationController < ActionController::Base
  protect_from_forgery
  
  rescue_from Exception, :with => :render_500 
  rescue_from ActiveRecord::RecordNotFound, :with => :render_404
  rescue_from ActiveRecord::UnknownAttributeError, :with => :render_400
  rescue_from ActiveModel::MassAssignmentSecurity::Error, :with => :render_403
  
  def error(status, code, message)
    render :js => {:response_type => "ERROR", :response_code => code, :message => message}.to_json, :status => status
  end
  
  def render_404(e)
    logger.error "#{e.message}\n\n#{e.backtrace.join("\n")}"
    render :file => "#{Rails.root}/public/404", :formats => :html, :status => :not_found
  end
  
  def render_400(e)
    logger.error "#{e.message}\n\n#{e.backtrace.join("\n")}"
    error(400, 400, "Attribute is not a part of the model")
  end

  def render_400_with_msg(msg)
    error(400, 400, msg)
  end

  def render_403(e)
    logger.error "#{e.message}\n\n#{e.backtrace.join("\n")}"
    error(400, 400, "The request was not accepted by the server.")
  end

  def render_500(e)
    logger.error "#{e.message}\n\n#{e.backtrace.join("\n")}"
    render :file => "#{Rails.root}/public/500", :formats => :html, :status => :internal_server_error
  end
  
  def append_to_query(queryStr, str)
    if queryStr != ''
      queryStr << ' AND '
    end
    
    queryStr << str
  end
end
