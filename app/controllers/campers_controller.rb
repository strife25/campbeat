class CampersController < AuthorizedController
  before_filter :is_camp_customer
  before_filter :get_camper, :only => [:show, :edit, :update, :destroy]
  
  # == GET /campers
  #
  # *NOTE*: This should only be accessible by devs/campbeat admins
  #
  # *TODO*: manage content-type of response by monitoring Accept header of the request
  # 
  # Returns a list of all the campers stored in the database. 
  #
  # === Response Codes: *TODO*
  #
  # * 200 OK
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML Response:
  #
  #  <campers>
  #   <camper> 
  #     <id>980190962</id> 
  #     <first_name>Billy</first_name> 
  #     <middle_name>A</middle_name> 
  #     <last_name>Joe</last_name> 
  #     <suffix></suffix> 
  #     <age>12</age> 
  #     <birthday>2001-03-28 22:05:27 UTC</birthday> 
  #     <grade>4</grade> 
  #     <gender>M</gender> 
  #     <camps> 
  #       <camp_uri>bolo</camp_uri> 
  #       <camp_uri>tester</camp_uri> 
  #     </camps> 
  #     <guardians> 
  #       <guardian> 
  #         <first_name>John</first_name> 
  #         <middle_name>L</middle_name> 
  #         <last_name>Doe</last_name> 
  #         <suffix></suffix> 
  #         <email_address>john@test.com</email_address> 
  #         <day_phone>1234567890</day_phone> 
  #         <evening_phone>1234567890</evening_phone> 
  #         <mobile_phone>1234567890</mobile_phone> 
  #       </guardian> 
  #       <guardian> 
  #         <first_name>Jane</first_name> 
  #         <middle_name>L</middle_name> 
  #         <last_name>Doe</last_name> 
  #         <suffix></suffix> 
  #         <email_address>jane@test.com</email_address> 
  #         <day_phone>1234567890</day_phone> 
  #         <evening_phone>1234567890</evening_phone> 
  #         <mobile_phone>1234567890</mobile_phone> 
  #       </guardian> 
  #     </guardians> 
  #     <address> 
  #       <street_1>92 McCormick Rd</street_1> 
  #       <street_2></street_2> 
  #       <city>Spencer</city> 
  #       <state>MA</state> 
  #       <zip_code>517</zip_code> 
  #       <country>USA</country> 
  #     </address> 
  #   </camper>
  #  </campers>
  #
  def index
    @campers = Camper.all
    
    if !@campers
      #call parent 404 method because no record was found
      #ActiveRecord::RecordNotFound is not being called
      self.render_404
    else
       respond_to do |format|
          format.html # index.html.erb
          format.xml  
          format.json { render :template => "campers/index.json" }
        end
    end
  end

  # == POST /campers
  #
  # Create a new camper resource.
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO*
  # * <tt>first-name {String}</tt> - The first name of the camper
  # * <tt>last-name {String}</tt> - The last name of the camper
  # * <tt>age {integer}</tt> - The age of the camper.
  # * <tt>birthday {UTC Formatted Date String}</tt> - The birth date of the camper.
  # * <tt>gender {char}</tt> - A char representing male (M) or female (F)
  #
  # *Optional*:
  # * <tt>suffix {String}</tt> - The suffix of the camper's name
  # * <tt>grade {Integer}</tt> - The school grade the camper is in
  # * <tt>address {Address Object}</tt> - The suffix of the camper's name
  # * <tt>parents {URI}</tt> - A list / array of parent resource URIs that are the camper's guardians
  # * <tt>emergency contacts {URI}</tt> - A list / array of parent resource URIs that will act as the camper's emergency contacts
  #
  # === Example XML Request:
  #
  #  <camper> 
  #     <first-name>Nathan</first-name>
  #     <middle-name>L</middle-name>
  #     <last-name>Gersbach</last-name>
  #     <suffix>Jr.</suffix>
  #     <age>12</age>
  #     <birthday>06-27-1999</birthday>
  #     <grade>6</grade>
  #     <gender>M</gender>
  #     <parents>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </parents>
  #     <emergency-contacts>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </emergency-contacts>
  #     <address>
  #          <street>501 Dehart St</street>
  #          <town>Blacksburg</town>
  #          <state>VA</state>
  #          <zip-code>24060</zip-code> 
  #          <country>USA</country>
  #     </address>
  # </camper>
  #
  # === Example XML Response:
  #
  #  <camper> 
  #     <first-name>Nathan</first-name>
  #     <middle-name>L</middle-name>
  #     <last-name>Gersbach</last-name>
  #     <suffix>Jr.</suffix>
  #     <age>12</age>
  #     <birthday>06-27-1999</birthday>
  #     <grade>6</grade>
  #     <gender>M</gender>
  #     <camps>
  #          <camp-uri>https://campbeat.com/camps/bolo</camp-uri> 
  #     </camps>
  #     <parents>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </parents>
  #     <emergency-contacts>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </emergency-contacts>
  #     <address>
  #          <street>501 Dehart St</street>
  #          <town>Blacksburg</town>
  #          <state>VA</state>
  #          <zip-code>24060</zip-code> 
  #          <country>USA</country>
  #     </address>
  # </camper>
  #
  def create
    @camper = Camper.new(params[:camper])

    respond_to do |format|
      if @camper.save
        current_user.campers << @camper
        
        format.html { render :text => "camper created!", :status => :created }
        format.xml  { render :xml => @camper, :status => :created, :location => @camper }
      else
        format.html { render :action => "new", :status => :bad_request }
        format.xml  { render :xml => @camper.errors, :status => :bad_request }
      end
    end
  end

  # == GET /campers/{id}
  #
  # Manages the retrieval of a single camper resource.
  #
  # === Response Codes: *TODO*
  #
  # * 200 Ok
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML response:
  #
  #    <camper> 
  #       <id>980190962</id> 
  #       <first_name>Billy</first_name> 
  #       <middle_name>A</middle_name> 
  #       <last_name>Joe</last_name> 
  #       <suffix></suffix> 
  #       <age>12</age> 
  #       <birthday>2001-03-28 22:05:27 UTC</birthday> 
  #       <grade>4</grade> 
  #       <gender>M</gender> 
  #       <camps> 
  #         <camp_uri>bolo</camp_uri> 
  #         <camp_uri>tester</camp_uri> 
  #       </camps> 
  #       <guardians> 
  #         <guardian> 
  #           <first_name>John</first_name> 
  #           <middle_name>L</middle_name> 
  #           <last_name>Doe</last_name> 
  #           <suffix></suffix> 
  #           <email_address>john@test.com</email_address> 
  #           <day_phone>1234567890</day_phone> 
  #           <evening_phone>1234567890</evening_phone> 
  #           <mobile_phone>1234567890</mobile_phone> 
  #         </guardian> 
  #         <guardian> 
  #           <first_name>Jane</first_name> 
  #           <middle_name>L</middle_name> 
  #           <last_name>Doe</last_name> 
  #           <suffix></suffix> 
  #           <email_address>jane@test.com</email_address> 
  #           <day_phone>1234567890</day_phone> 
  #           <evening_phone>1234567890</evening_phone> 
  #           <mobile_phone>1234567890</mobile_phone> 
  #         </guardian> 
  #       </guardians> 
  #       <address> 
  #         <street_1>92 McCormick Rd</street_1> 
  #         <street_2></street_2> 
  #         <city>Spencer</city> 
  #         <state>MA</state> 
  #         <zip_code>517</zip_code> 
  #         <country>USA</country> 
  #       </address> 
  #     </camper>
  #
  def show
    respond_to do |format|
      format.html
      format.xml  
      format.json { render :template => "campers/show.json" }
    end
  end
  
  # == PUT /camps/{id}
  # 
  # Update the data of a camper resource
  #
  # === Response Codes:
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Optional*:
  # * <tt>first-name {String}</tt> - The first name of the camper
  # * <tt>last-name {String}</tt> - The last name of the camper
  # * <tt>suffix {String}</tt> - The suffix of the camper's name
  # * <tt>age {integer}</tt> - The age of the camper.
  # * <tt>birthday {UTC Formatted Date String}</tt> - The birth date of the camper.
  # * <tt>grade {Integer}</tt> - The school grade the camper is in
  # * <tt>gender {char}</tt> - A char representing male (M) or female (F)
  # * <tt>address {Address Object}</tt> - The suffix of the camper's name
  # * <tt>parents {URI}</tt> - A list / array of parent resource URIs that are the camper's guardians
  # * <tt>emergency contacts {URI}</tt> - A list / array of parent resource URIs that will act as the camper's emergency contacts
  #
  # === Example XML Request:
  #
  #  <camper> 
  #     <first-name>Nathan</first-name>
  #     <middle-name>L</middle-name>
  #     <last-name>Gersbach</last-name>
  #     <suffix>Jr.</suffix>
  #     <age>12</age>
  #     <birthday>06-27-1999</birthday>
  #     <grade>6</grade>
  #     <gender>M</gender>
  #     <parents>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </parents>
  #     <emergency-contacts>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </emergency-contacts>
  #     <address>
  #          <street>501 Dehart St</street>
  #          <town>Blacksburg</town>
  #          <state>VA</state>
  #          <zip-code>24060</zip-code> 
  #          <country>USA</country>
  #     </address>
  # </camper>
  #
  # === Example XML Response:
  #
  #  <camper> 
  #     <first-name>Nathan</first-name>
  #     <middle-name>L</middle-name>
  #     <last-name>Gersbach</last-name>
  #     <suffix>Jr.</suffix>
  #     <age>12</age>
  #     <birthday>06-27-1999</birthday>
  #     <grade>6</grade>
  #     <gender>M</gender>
  #     <camps>
  #          <camp-uri>https://campbeat.com/camps/bolo</camp-uri> 
  #     </camps>
  #     <parents>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </parents>
  #     <emergency-contacts>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #          <parent-uri>https://campbeat.com/parents/fn48tfhsu<parent-uri>
  #     </emergency-contacts>
  #     <address>
  #          <street>501 Dehart St</street>
  #          <town>Blacksburg</town>
  #          <state>VA</state>
  #          <zip-code>24060</zip-code> 
  #          <country>USA</country>
  #     </address>
  # </camper>
  #
  def update
    if !@camper
      #call parent 404 method because no record was found
      #ActiveRecord::RecordNotFound is not being called
      self.render_404 
    else
      respond_to do |format|
        if @camper.update_attributes(params[:camper])
          format.html { render :text => "you have updated the camper model", :status => :accepted }
          format.xml  { head :accepted }
        else
          format.html { render :action => "edit", :status => :bad_request }
          format.xml  { render :xml => @camper.errors, :status => :bad_request }
        end
      end
    end
  end

  # == DELETE /campers/{id}
  #
  # Delete a camper resource
  #
  # === Response Codes: *TODO*
  #
  # * 202 Accepted
  # * 401 Unauthorized
  # * 404 Not Found
  #
  def destroy
    @camper.destroy

    respond_to do |format|
      format.html { render :text => "camper destroyed!", :status => :accepted }
      format.xml  { head :accepted }
    end
  end
  
  # GET /campers/new
  # GET /campers/new.xml
  def new
    @camper = Camper.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @camper }
    end
  end

  # GET /campers/1/edit
  def edit
    if not (@camp.nil? && @camper.nil?)
      respond_to do |format|
        format.html
      end
    end
  end
  
  private
    def is_camp_customer
      if not (current_user.camp_customer? || current_user.cb_admin? )
        respond_to do |format|
          format.html { redirect_to root_url, :status => :unauthorized }
          format.xml  { redirect_to root_url, :status => :unauthorized }
          format.json { redirect_to root_url, :status => :unauthorized }
        end
      end
    end
    
    def get_camper
      @camper = Camper.find(params[:id]) 
      
      if !@camper
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_404
      end  
    end
end
