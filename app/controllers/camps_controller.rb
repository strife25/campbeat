# Manages the Camps resource service.
class CampsController < AuthorizedController
  skip_load_resource
  before_filter :get_camp, :only => [:show, :edit, :update, :destroy]
  authorize_resource
   
  # == GET /camps
  #
  # *NOTE*: This should only be accessible by devs/campbeat admins
  #
  # *TODO*: manage content-type of response by monitoring Accept header of the request
  # 
  # Returns a list of all the camps stored in the database. 
  #
  # === Response Codes: *TODO*
  #
  # * 200 OK
  # * 401 Unauthorized
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML Response:
  #
  #  <camps>
  #    # start camp resource
  #    <camp>
  #      <id>1</id>
  #      <uri>bolo</uri>
  #      <name>Camp Bolo</name>
  #      <web_address>http://www.campbolo.com</web_address>
  #      <email_address>contact@campbolo.com</email_address>
  #      <address>
  #        <street_1>501 Dehart St</street_1>
  #        <street_2>501 Dehart St</street_2>
  #        <city>Blacksburg</city>
  #        <state>VA</state>
  #        <zip_code>24060</zip_code> 
  #        <country>USA</country>
  #      </address>
  #      <contacts>
  #        <contact>
  #          <first_name>Tim</first_name> 
  #          <middle_name>J</middle_name> 
  #          <last_name>Tutt</last_name> 
  #          <suffix>Jr.</suffix> 
  #          <email_address>ttutt@vt.edu</email_address>
  #          <day_phone>12345678901</day_phone>
  #          <evening_phone>12345678901</evening_phone>
  #          <mobile_phone>12345678901</mobile_phone>
  #        </contact>
  #      </contacts>
  #    </camp>
  #    # end camp resource
  #  </camps>
  #
  def index
    @camps = Camp.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml 
      format.json { render :template => "camps/index.json" }
    end
  end
  
  # == POST /camps
  #
  # Create a new camp resource.
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO*
  # * <tt>uri {String}</tt> - A unique identifier that defines the URI root of the camp.  This string cannot be duplicated by any other camp within the system.
  # * <tt>name {String}</tt> - The actual name of the camp, e.g. Camp Bolo.
  # * <tt>address {Object}</tt> - An object representation of a U.S. address.
  #
  # *Optional*:
  # * <tt>web_address {URI String}</tt> - The web address of the camp's website.
  # * <tt>email_address {Email Address String}</tt> -  The email address of the camp, e.g. "contact@campbolo.com". 
  #
  # === Example XML Request:
  #
  #  <camp>
  #    <uri>bolo</uri>
  #    <name>Camp Bolo</name>
  #    <web_address>http://www.campbolo.com</web_address>
  #    <email_address>contact@campbolo.com</email_address>
  #    <address>
  #      <street>501 Dehart St</street>
  #      <city>Blacksburg</city>
  #      <state>VA</state>
  #      <zip-code>24060</zip-code> 
  #      <country>USA</country>
  #    </address>
  #  </camp>
  #
  # === Example XML Response:
  #
  #  <camp>
  #    <id>1</id>
  #    <uri>bolo</uri>
  #    <name>Camp Bolo</name>
  #    <web_address>http://www.campbolo.com</web_address>
  #    <email_address>contact@campbolo.com</email_address>
  #    <address>
  #      <street>501 Dehart St</street>
  #      <city>Blacksburg</city>
  #      <state>VA</state>
  #      <zip-code>24060</zip-code> 
  #      <country>USA</country>
  #    </address>
  #    <contacts>
  #      <contact>
  #        <first-name>Tim</first-name> 
  #        <middle-name>J</middle-name> 
  #        <last-name>Tutt</last-name> 
  #        <suffix>Jr.</suffix> 
  #        <email-address>ttutt@vt.edu</email-address>
  #        <phone-number>12345678901</phone-number>
  #      </contact>
  #    </contacts>
  #  </camp>
  #
  def create
    @camp = Camp.new(params[:camp])
    
    respond_to do |format|
      if @camp.save
        current_user.camps << @camp
        format.html { render :text => "camp created!", :status => :created }
        format.xml {render :xml => @camp, :status => :created, :location => @camp}
      else
        format.html { render :action => "new", :status => :bad_request }
        format.xml  { render :xml => @camp.errors, :status => :bad_request }
      end
    end
  end

  # == GET /camps/{id}
  #
  # Manages the retrieval of a single camp resource.
  #
  # === Response Codes: *TODO*
  #
  # * 200 Ok
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML response:
  #
  #    <camp>
  #      <id>1</id>
  #      <uri>bolo</uri>
  #      <name>Camp Bolo</name>
  #      <web_address>http://www.campbolo.com</web_address>
  #      <email_address>contact@campbolo.com</email_address>
  #      <address>
  #        <street_1>501 Dehart St</street_1>
  #        <street_2>501 Dehart St</street_2>
  #        <city>Blacksburg</city>
  #        <state>VA</state>
  #        <zip_code>24060</zip_code> 
  #        <country>USA</country>
  #      </address>
  #      <contacts>
  #        <contact>
  #          <first_name>Tim</first_name> 
  #          <middle_name>J</middle_name> 
  #          <last_name>Tutt</last_name> 
  #          <suffix>Jr.</suffix> 
  #          <email_address>ttutt@vt.edu</email_address>
  #          <day_phone>12345678901</day_phone>
  #          <evening_phone>12345678901</evening_phone>
  #          <mobile_phone>12345678901</mobile_phone>
  #        </contact>
  #      </contacts>
  #    </camp>
  #
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  # show.xml.builder
      format.json { render :template => "camps/show.json" }
    end
  end
  
  # == PUT /camps/{id}
  # 
  # Update the data of a camp resource
  #
  # === Response Codes:
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Optional*:
  # * <tt>name {String}</tt> - The actual name of the camp, e.g. Camp Bolo.
  # * <tt>address {Object}</tt> - An object representation of a U.S. address.
  # * <tt>web_address {URI String}</tt> - The web address of the camp's website.
  # * <tt>emailAddress {Email Address String}</tt> -  The email address of the camp, e.g. "contact@campbolo.com". 
  #
  # === Example XML request:
  #
  #  <camp>
  #    <name>Camp Bolo</name>
  #    <web_address>http://www.campbolo.com</web_address>
  #    <email_address>contact@campbolo.com</email_address>
  #    <address>
  #      <street_1>92 McCormick Rd</street_1>
  #      <street_2></street_2>
  #      <city>Spencer</city>
  #      <state>MA</state>
  #      <zip-code>01005</zip-code> 
  #      <country>USA</country>
  #    </address>
  #  </camp>
  #
  # === Example XML Response:
  #
  #  <camp>
  #    <id>1</id>
  #    <uri>bolo</uri>
  #    <name>Camp Bolo</name>
  #    <web_address>http://www.campbolo.com</web_address>
  #    <email_address>contact@campbolo.com</email_address>
  #    <address>
  #      <street_1>92 McCormick Rd</street_1>
  #      <street_2></street_2>
  #      <city>Spencer</city>
  #      <state>MA</state>
  #      <zip-code>01005</zip-code> 
  #      <country>USA</country>
  #    </address>
  #    <contacts>
  #      <contact>
  #        <first_name>Tim</first_name> 
  #        <middle_name>J</middle_name> 
  #        <last_name>Tutt</last_name> 
  #        <suffix>Jr.</suffix> 
  #        <email_address>ttutt@vt.edu</email_address>
  #        <phone_number>12345678901</phone_number>
  #      </contact>
  #    </contacts>
  #  </camp>
  #
  def update
    respond_to do |format|
      if @camp.update_attributes(params[:camp])
        format.html { render :text => "you have updated the camp model", :status => :accepted }
        format.xml  { head :accepted }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @camp.errors, :status => :bad_request }
      end
    end
  end

  # == DELETE /camps/{id}
  #
  # Delete a camp resource
  #
  # === Response Codes: *TODO*
  #
  # * 202 Accepted
  # * 401 Unauthorized
  # * 404 Not Found
  #
  def destroy
      @camp.destroy
      respond_to do |format|
        format.html { render :text => "camp destroyed!", :status => :accepted }
        format.xml  { head :accepted }
      end
  end

  # GET /camps/new
  # TODO: Remove when we have a UI
  def new
    @camp = Camp.new
    @contact = @camp.contacts.build
    @address = @camp.address
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @camp }
    end
  end

  # GET /camps/1/edit
  # TODO: Remove when we have a UI
  def edit
    @camp = Camp.find_by_uri(params[:id])
  end
  
  private
    def get_camp
      @camp = Camp.find_by_uri(params[:id])
      
      if !@camp
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_404
      end
    end


end
