class Camp::SessionsController < AuthorizedController
  skip_load_and_authorize_resource
  before_filter :get_camp
  before_filter :get_session, :only => [:show, :edit, :update, :destroy]
  authorize_resource :camp
  load_and_authorize_resource :session, :through => :camp
  
  # == GET /camps/{camp_id}/sessions
  #
  # Get a list of the sessions that are offered by the camp
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  # * 409 Conflict
  #
  # === Supported Content Types:
  #
  # * *TODO* XML
  # * *TODO* JSON
  #
  # === Parameters:
  #
  # *Optional*:
  # * <tt>start_date {UTC Date}</tt> - *TODO* Returns all sessions that are available on or after the given date
  # * <tt>end_date {UTC Date}</tt> - *TODO* Returns all sessions that are available on or before the given date
  # * <tt>program_name {String}</tt> - *TODO* Returns a list of all sessions that contain the given program
  # * <tt>open {booleam}</tt> - *TODO* If set to TRUE, returns all sessions whose current_population != max_population
  # * <tt>closed {boolean}</tt> - *TODO* If set to TRUE, returns all sessions whose current_population == max_population
  #
  # === Example XML Response:
  #
  #  <sessions>
  #    <session>
  #       <id>e32jri84f8</id>
  #       <camp_uri>bolo</camp_uri>
  #       <start_date>2010-08-21 18:43:00</start_date>
  #       <end_date>2010-08-21 18:43:00</end_date>
  #       <programs>
  #         <program_id>https://campbeat.com/bolo/programs/r4iohrs8th</program_id>
  #         <program_id>https://campbeat.com/bolo/programs/fjdfrjbr</program_id>
  #         <program_id>https://campbeat.com/bolo/programs/fejhfurigb</program_id>
  #       </programs>
  #    </session>
  #  <sessions>
  #
  def index
    query = ''
    append_to_query(query, 'start_date >= :start_date') if params[:start_date]
    append_to_query(query, 'end_date <= :end_date') if params[:end_date]
    
    if (query.length > 0)
      @sessions = @camp.sessions.where(query, params).order('start_date')
    else
      @sessions = @camp.sessions
    end
    
    @sessions = @sessions.joins(:programs).where("programs.name = ?", params[:program_name]) if params[:program_name]
    @sessions = @sessions.send(params[:availablity].to_sym) if ['vacant', 'closed'].include?(params[:availablity])

    respond_to do |format|
      format.html # index.html.erb
      format.xml
      format.json { render :template => "camp/sessions/index.json" }
    end
  end
  
  # == POST /camps/{camp_id}/sessions
  #
  # Create a new session resource for a camp
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO* 
  # * <tt>start_date {UTC Date}</tt> - The start date of the session.
  # * <tt>end_date {UTC Date}</tt> - The end date of the session.
  #
  # === Example Request:
  #  <session>
  #    <id>e32jri84f8</id>
  #    <start_date>2010-08-21 18:43:00</start_date>
  #    <end_date>2010-08-21 18:43:00</end_date>
  #  </session>
  #
  def create
    @session = @camp.sessions.build(params[:session])

    respond_to do |format|
      if @session.save
        format.html { render :text => "session created!", :status => :created }
        format.xml {render :xml => @session , :status => :created, :location => @session }
      else
        format.html { render :action => "new", :status => :bad_request }
        format.xml  { render :xml => @session.errors, :status => :bad_request }
      end
    end
  end

  # == GET /camps/{camp_id}/sessions/{id}
  #
  # Manages the retrieval of a session resource
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML response:
  #  <session>
  #       <id>e32jri84f8</id>
  #       <camp_uri>bolo</camp_uri>
  #       <start_date>2010-08-21 18:43:00</start_date>
  #       <end_date>2010-08-21 18:43:00</end_date>
  #       <programs>
  #         <program_id>https://campbeat.com/bolo/programs/r4iohrs8th</program_id>
  #         <program_id>https://campbeat.com/bolo/programs/fjdfrjbr</program_id>
  #         <program_id>https://campbeat.com/bolo/programs/fejhfurigb</program_id>
  #       </programs>
  #    </session>
  #
  def show
    if @camper.nil?
      #respond to request made to /camps/{camp uri id}/sessions/{id}
      respond_to do |format|
        format.html # show.html.erb
        format.xml
        format.json { render :template => "camp/sessions/show.json" }
      end
    else
      #respond to request made to /camps/{camp uri id}/campers/{camper_id/sessions/{id}
      @program = @camper.programs.first(:conditions => ['session_id = ?', @session.id])
      respond_to do |format|
        format.xml  { render :template => 'camps/campers/sessions/show'}
      end
    end
    
  end
  
  # == PUT /camps/{camp_id}/sessions/{id}
  #
  # Update the data of a session resource
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  # *Optional*:
  # * <tt>start_date {UTC Date}</tt> - The start date of the session.
  # * <tt>end_date {UTC Date}</tt> - The end date of the session.
  #
  # === Example XML request:
  #  <session>
  #    <id>e32jri84f8</id>
  #    <start_date>2010-08-21 18:43:00</start_date>
  #    <end_date>2010-08-21 18:43:00</end_date>
  #  </session>
  #
  def update
    if !@session
      #call parent 404 method because no record was found
      #ActiveRecord::RecordNotFound is not being called
      self.render_404 
    else
      respond_to do |format|
        if @session.update_attributes(params[:session])
          format.html { render :text => "you have updated the program model", :status => :accepted }
          format.xml  { head :accepted }
        else
          format.html { render :action => "edit", :status => :bad_request }
          format.xml  { render :xml => @session.errors, :status => :bad_request }
        end
      end
    end
  end

  # DELETE /camps/{camp_id}/sessions/{id}
  #
  # Delete a session resource
  #
  # === Response Codes: *TODO*
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  def destroy
    @session.destroy

    respond_to do |format|
      format.html { render :text => "session destroyed!", :status => :accepted }
      format.xml  { head :accepted }
    end
  end

  # GET /sessions/new
  # GET /sessions/new.xml
  def new
    @session = @camp.sessions.build

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @session }
    end
  end

  # GET /sessions/1/edit
  def edit
    if not (@camper.nil?)
      respond_to do |format|
        format.html  { render :template => 'camp/sessions/edit'}
      end
    end
  end
  
  private
    def get_camp
      @camp = Camp.find_by_uri(params[:camp_id])
      if !@camp
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_400 
      end
    end
    
    def get_session
        @session = @camp.sessions.find(params[:id])
    end
end
