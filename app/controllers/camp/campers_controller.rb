class Camp::CampersController < AuthorizedController
  skip_load_and_authorize_resource
  before_filter :get_camp
  before_filter :get_campers, :only => [:index]
  before_filter :get_camper, :only => [:show, :edit, :update, :destroy]
  authorize_resource :camp
  load_and_authorize_resource :camper, :through => :camp
  
  # == GET /camps/{camp_uri}/campers
  #
  # *TODO*: manage content-type of response by monitoring Accept header of the request
  # 
  # Returns a list of all the campers attending the camp referenced by {camp_uri}
  #
  # === Response Codes: *TODO*
  #
  # * 200 OK
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Parameters:
  #
  # *Optional*:
  # * <tt>name {String}</tt> - The name of a camper
  # * <tt>min_age {Integer}</tt> - Returns all campers that are greater than or equal to the given age
  # * <tt>max_age {Integer}</tt> - Returns all campers that are less than or equal to the given age
  # * <tt>grade {Integer}</tt> - Returns all campers that are in the given school grade
  # * <tt>gender {Char}</tt> - Returns all campers that are of the given gender. Can be M or F.
  # * <tt>cabin {String}</tt> - *TODO* Returns all campers that are in the given cabin.
  # * <tt>program_id {String}</tt> - Using a program id, Returns a list of all campers attending the given program
  # * <tt>program_name {String}</tt> - Using a program's name, Returns a list of all campers attending the given program
  # * <tt>session_id {String}</tt> - Using a session id, Returns a list of all campers attending the given session
  #
  # === Example XML Response:
  #    <campers>
  #      <camper>
  #        <id>fw484w4wh</id> 
  #        <first-name>Nathan</first-name>
  #        <middle-name>L</middle-name>
  #        <last-name>Gersbach</last-name>
  #        <suffix>Jr.</suffix>
  #        <age>12</age>
  #        <birthday>06-27-1999</birthday>
  #        <grade>6</grade>
  #        <gender>M</gender>
  #        <guardians>
  #            <guardian>https://vertizu.com/parents/fn48tfhsu<guardian>
  #            <guardian>https://vertizu.com/parents/fn48tfhsu<guardian>
  #        </guardians>
  #        <address>
  #            <street>501 Dehart St</street>
  #            <town>Blacksburg</town>
  #            <state>VA</state>
  #            <zip-code>24060</zip-code> 
  #            <country>USA</country>
  #       </address>
  #       <sessions>
  #            <session-id>f48wfh8</session-id> 
  #       </sessions>
  #      </camper>
  #    <campers>
  #
  def index
    query = ''
    append_to_query(query, 'grade = :grade') if params[:grade]
    append_to_query(query, 'gender = :gender') if ['M', 'F', 'm', 'f'].include?(params[:gender])

    if (query.length > 0)
      @campers = @camp.campers.where(query, params)
    else
      @campers = @camp.campers
    end

    # '<=' because we are comparing by years, so min age will determine the max year in which a person is born
    # vice versa for max_age
    @campers = @campers.where( "birthday <= ?", Time.utc(Time.now.year - params[:min_age].to_i) ) if params[:min_age]
    @campers = @campers.where( "birthday >= ?", Time.utc(Time.now.year - params[:max_age].to_i) ) if params[:max_age]
    
    @campers = @campers.joins(:programs).where('programs.id = ?', params[:program_id]) if params[:program_id]
    @campers = @campers.joins(:programs).where('programs.name = ?', params[:program_name]) if params[:program_name]
    @campers = @campers.joins(:sessions).where('sessions.id = ?', params[:session_id]) if params[:session_id]
      
    if @campers
      respond_to do |format|
        format.html # index.html.erb
        format.xml
        format.json{ render :template => "camp/campers/index", :formats => [:json] }
      end
    else
      respond_to do |format|
        format.html { render :action => "index", :status => :bad_request }
        format.xml  { render :xml => @campers.errors, :status => :bad_request }
      end
    end
  end

  # == POST /camps/{camp_uri}/campers
  #
  # Register a camper with the camp.
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO* 
  # * <tt>program-id {String}</tt> - The id of the program the camper will attend
  # * <tt>session-id {String}</tt> - The id of the session the programmer will attend
  #
  # === Example XML Response:
  #
  #    <camper>
  #        <id>fw484w4wh</id>
  #        <programs> 
  #            <program>
  #                <program_id>dn3iwrh8</program_id>
  #                <session_id>f48wfh8</session_id> 
  #            </program>
  #        <programs>
  #    </camper>
  #
  def create
    if @camp.campers.exists?(params[:camper_id])
      error(409, 409, "The camper is already registered with the camp")
    else
      @camper = Camper.find(params[:camper_id])

      if params[:programs].nil?
        respond_to do |format|
          format.html { render :action => "new", :status => :bad_request }
          format.xml  { render :xml => @camper.errors, :status => :bad_request }
        end
      else
        resultHash = register_camper_with_program(params[:programs])
        
        unless resultHash[:success].empty?
           @camp.campers << @camper

            respond_to do |format|
                format.html { render :text => "camper registered!", :status => :created }
                format.xml {render :status => :created, :location => @camper}
            end
        else 
          respond_to do |format|
              format.html { render :action => "new", :status => :bad_request }
              format.xml {render :status => :bad_request, :location => @camper}
          end
        end
      end
    end
  end

  # == GET /camps/{camp_uri}/campers/{camper_id}
  #
  # *TODO*: manage content-type of response by monitoring Accept header of the request
  # 
  # Returns a list of all the campers attending the camp referenced by {camp_uri}
  #
  # === Response Codes: *TODO*
  #
  # * 200 OK
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML Response:
  #
  #      <camper>
  #        <id>fw484w4wh</id> 
  #        <first-name>Nathan</first-name>
  #        <middle-name>L</middle-name>
  #        <last-name>Gersbach</last-name>
  #        <suffix>Jr.</suffix>
  #        <age>12</age>
  #        <birthday>06-27-1999</birthday>
  #        <grade>6</grade>
  #        <gender>M</gender>
  #        <parents>
  #            <parent-uri>https://vertizu.com/parents/fn48tfhsu<parent-uri>
  #            <parent-uri>https://vertizu.com/parents/fn48tfhsu<parent-uri>
  #        </parents>
  #        <emergency-contacts>
  #            <parent-uri>https://vertizu.com/parents/fn48tfhsu<parent-uri>
  #            <parent-uri>https://vertizu.com/parents/fn48tfhsu<parent-uri>
  #        </emergency-contacts>
  #        <address>
  #            <street>501 Dehart St</street>
  #            <town>Blacksburg</town>
  #            <state>VA</state>
  #            <zip-code>24060</zip-code> 
  #            <country>USA</country>
  #       </address>
  #       <sessions>
  #            <session-id>f48wfh8</session-id> 
  #       </sessions>
  #      </camper>
  #
  def show
    if !@camper
      #call parent 404 method because no record was found
      #ActiveRecord::RecordNotFound is not being called
      self.render_404 
    else
      
      @sessions = @camper.sessions.where('camp_id = ?', @camp.id)
      
      respond_to do |format|
        format.html # show.html.erb
        format.xml  # show.xml.builder
        format.json{ render :template => "camp/campers/show", :formats => [:json], :handlers => :erb }
      end
    end
  end

  # == PUT /camps/{camp_uri}/campers
  #
  # Update the sessions and programs that the camper is attending.
  # A request to this service will add programs and sessions
  # the user is attending.
  #
  # === Response Codes: *TODO*
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO* 
  # * <tt>program-id {String}</tt> - The id of the program the camper will attend
  # * <tt>session-id {String}</tt> - The id of the session the programmer will attend
  #
  # === Example XML Response:
  #
  #    <camper>
  #        <id>fw484w4wh</id>
  #        <programs> 
  #            <program>
  #                <program-id>dn3iwrh8</program-id>
  #                <session-id>f48wfh8</session-id> 
  #            </program>
  #        <programs>
  #    </camper>
  #
  def update
    if params[:programs].nil?
      respond_to do |format|
        format.html { render :action => "edit", :status => :bad_request }
        format.xml  { render :xml => @camper.errors, :status => :bad_request }
      end
    else
      resultHash = register_camper_with_program(params[:programs])

      unless resultHash[:success].empty?
          respond_to do |format|
            format.html { render :text => "you have updated the registered camper", :status => :accepted }
            format.xml  { head :accepted }
          end
      else 
        respond_to do |format|
            format.html { render :action => "edit", :status => :bad_request }
            format.xml {render :xml => @camper, :status => :bad_request, :location => @camper}
        end
      end
    end
  end

  def edit
    if not (@camper.nil?)
      respond_to do |format|
        format.html  { render :template => 'camp/campers/edit'}
      end
    end
  end
  
  private
    def get_camp
      @camp = Camp.find_by_uri(params[:camp_id])
      if !@camp
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_404
      end
    end
    
    def get_camper
      @camper = @camp.campers.find(params[:id])
      if !@camper
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_404
      end
    end
    
    def get_campers
      @campers = @camp.campers
    end
    
    def register_camper_with_program(programs)
      result = {
        :success => Array.new,
        :failed => Array.new
      }
      
      camperAge = @camper.age
      camperId = @camper.id
      
      programs.each do |programParam|
        programId = programParam[:program_id]
        sessionId = programParam[:session_id]

        if CamperAttendance.exists?([ "camper_id = ? AND program_id = ? AND session_id = ?", camperId, programId, sessionId ])
          result[:failed] << {
            :program_id => programId,
            :session_id => sessionId
          }
          
          next
        end
        
        unless @camp.programs.exists?(programId) 
          result[:failed] << {
            :program_id => programId,
            :session_id => sessionId
          }
          
          next
        end
        
        programObj = Program.find(programId)
        unless programObj.is_valid_age(camperAge)
          result[:failed] << {
            :program_id => programId,
            :session_id => sessionId
          }
          
          next
        end
        
        pop = ProgramPopulation.where("program_id = ? AND session_id = ?", programId, sessionId).limit(1)[0]
        
        unless pop.nil? or !pop.is_open
          pop.increment!(:current_population)

          @camper.camper_attendances.create(
            :camper_id => camperId,
            :program_id => programId,
            :session_id => sessionId
          )
          
          result[:success] << {
            :program_id => programId,
            :session_id => sessionId
          }
        else
          result[:failed] << {
            :program_id => programId,
            :session_id => sessionId
          }
        end
      end
      
      return result
    end
end
