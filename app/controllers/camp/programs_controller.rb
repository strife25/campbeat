class Camp::ProgramsController < AuthorizedController
  skip_load_and_authorize_resource
  before_filter :get_camp
  before_filter :get_camp_program, :only => [:show, :edit, :update, :destroy]
  authorize_resource :camp
  load_and_authorize_resource :program, :through => :camp
  
  # == GET /camps/{camp_id}/programs
  # 
  # Returns a list of all the programs that are offered by a camp, e.g. Horse Camp, CIT, regular, etc.
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types:
  #
  # * *TODO* XML
  # * *TODO* JSON
  #
  # === Parameters:
  #
  # *Optional*:
  # * <tt>min_age {int}</tt> - Returns all programs that have campers with the given minimum age.
  # * <tt>max_age {int}</tt> - Returns all programs that have campers with the given maximum age.
  # * <tt>min_price {decimal}</tt> - Returns all programs that are offered at or above the given cost.
  # * <tt>max_price {decimal}</tt> - Returns all programs that are offered at or below the given cost.
  # * <tt>start_date {UTC Date}</tt> - Returns all programs that are available on or after the given date
  # * <tt>end_date {UTC Date}</tt> - Returns all programs that are available on or before the given date
  # * <tt>availability</tt> - will either be the values "open" or "closed" and will return the programs, that have sessions that are open or closed
  #
  # === Example XML Response:
  #
  #  <programs>
  #    <program>
  #       <id>e32jri84f8</id>
  #       <camp_uri>bolo</camp_uri>
  #       <name>Traditional Camp</name>
  #       <description>A traditional summer camp experience</description>
  #       <min_age>10</min-age>
  #       <max_age>15</max-age>
  #       <price>$385</price>
  #       <sessions>
  #         <session>
  #           <session_id>r4iohrs8th<session_id>
  #           <current_population>10</current_population>
  #           <max_population>300</max_population>
  #         </session>
  #       </sessions>    
  #     </program>
  #   <programs>
  #
  def index
    query = ''
    append_to_query(query, 'min_age >= :min_age') if params[:min_age]
    append_to_query(query, 'max_age <= :max_age') if params[:max_age]
    append_to_query(query, 'price >= :min_price') if params[:min_price]
    append_to_query(query, 'price <= :max_price') if params[:max_price]
    
    if (query.length > 0)
      @programs = @camp.programs.where(query, params).order('name')
    else
      @programs = @camp.programs
    end
    
    @programs = @programs.joins(:sessions).where('sessions.start_date >= ?', params[:start_date].to_date) if params[:start_date]
    @programs = @programs.joins(:sessions).where('sessions.end_date <= ?', params[:end_date].to_date) if params[:end_date]
    @programs = @programs.send(params[:availablity].to_sym) if ['vacant', 'closed'].include?(params[:availablity])
    
    if @programs
      respond_to do |format|
        format.html # index.html.erb
        format.xml
        format.json { render :template => "camp/programs/index.json" }
      end
    else
      respond_to do |format|
        format.html { render :action => "index", :status => :not_found }
        format.xml  { render :xml => @programs.errors, :status => :bad_request }
      end
    end
  end
  
  # == POST /camps/{camp_id}/programs
  #
  # Create a new program resource for a camp
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO* 
  # * <tt>name {String}</tt> - The name of the program.
  #
  # *Optional*:
  # * <tt>description {String}</tt> - The summary of what occurs in a program.
  # * <tt>min age {int}</tt> -  The minimum age a camper must be to be part of program
  # * <tt>max age {int}</tt> -  The maximum age a camper must be to be part of program
  # * <tt>cost {decimal}</tt> -  The financial cost that a camper must pay to be a part of the program
  # * <tt>sessions {Object Array / List}</tt> - A list of Camp Session resource URIs that the program occurs in and the populations of the program for that given session
  # 
  # === Example Request:
  #    <program>
  #       <name>Traditional Camp</name>
  #       <description>A traditional summer camp experience</description>
  #       <min_age>10</min-age>
  #       <max_age>15</max-age>
  #       <cost>$385</cost> 
  #     </program>
  #     <sessions>
  #        <session>
  #           <session_id>r4iohrs8th<session_id>
  #           <max_population>300</max_population>
  #        </session>
  #     </sessions>
  #
  def create
    @program = @camp.programs.build(params[:program])
    
    respond_to do |format|
      if @program.save
        params[:sessions].each do |sessionParam|
          session = @camp.sessions.find(sessionParam[:session_id])
          
          @program.program_populations.create(
            :program_id => @program.id,
            :session_id => session.id,
            :max_population => sessionParam[:max_population]
          )
        end
        
        format.html { render :text => "program created!", :status => :created }
        format.xml {render :xml => @program, :status => :created, :location => @program}
      else
        format.html { render :action => "new", :status => :bad_request }
        format.xml  { render :xml => @program.errors, :status => :bad_request }
      end
    end
  end
  
  # == GET /camps/{camp_id}/programs/{id}
  #
  # Manages the retrieval of a program resource
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML response:
  #
  #     <program>
  #       <id>e32jri84f8</id>
  #       <camp_uri>bolo</camp_uri>
  #       <name>Traditional Camp</name>
  #       <description>A traditional summer camp experience</description>
  #       <min_age>10</min-age>
  #       <max_age>15</max-age>
  #       <price>$385</price>
  #       <sessions>
  #         <session>
  #           <session_id>r4iohrs8th<session_id>
  #           <current_population>10</current_population>
  #           <max_population>300</max_population>
  #         </session>
  #       </sessions>    
  #     </program>
  #
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml #show.xml.builder
      format.json { render :template => "camp/programs/show.json" }
    end
  end
  
  # == PUT /camps/{camp_id}/programs/{id}
  #
  # Update the data of a program resource
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Optional*:
  # * <tt>name {String}</tt> - The name of the program.
  # * <tt>description {String}</tt> - The summary of what occurs in a program.
  # * <tt>min age {int}</tt> -  The minimum age a camper must be to be part of program
  # * <tt>max age {int}</tt> -  The maximum age a camper must be to be part of program
  # * <tt>cost {decimal}</tt> -  The financial cost that a camper must pay to be a part of the program
  # * <tt>sessions {Object Array / List}</tt> - A list of Camp Session resource URIs that the program occurs in and the populations of the program for that given session
  #
  # === Example XML request:
  #
  #  <program>
  #    <name>Traditional Camp</name>
  #    <description>A traditional summer camp experience</description>
  #    <min_age>10</min-age>
  #    <max_age>15</max-age>
  #    <cost>$385</cost> 
  #  </program>
  #   <sessions>
  #      <session>
  #        <session_url>https://campbeat.com/bolo/sessions/r4iohrs8th<session_url>
  #        <max_population>300</max_population>
  #      </session>
  #    </sessions>
  #
  def update
    if !@program
      #call parent 404 method because no record was found
      #ActiveRecord::RecordNotFound is not being called
      self.render_404 
    else
      respond_to do |format|
        if @program.update_attributes(params[:program])
          
          @program.program_populations.destroy_all
          params[:sessions].each do |sessionParam|
            session = @camp.sessions.find(sessionParam[:session_id])

            @program.program_populations.create(
              :program_id => @program.id,
              :session_id => session.id,
              :max_population => sessionParam[:max_population]
            )
          end
          
          format.html { render :text => "you have updated the program model", :status => :accepted }
          format.xml  { head :accepted }
        else
          format.html { render :action => "edit", :status => :bad_request }
          format.xml  { render :xml => @program.errors, :status => :bad_request }
        end
      end
    end
  end

  # == DELETE /camps/{camp_id}/programs/{id}
  #
  # Delete a program resource
  #
  # === Response Codes: *TODO*
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  def destroy
    @program.destroy

    respond_to do |format|
      format.html { render :text => "program destroyed!", :status => :accepted }
      format.xml  { head :accepted }
    end
  end

  

  # GET /programs/new
  # GET /programs/new.xml
  def new
    @program = @camp.programs.build
    @program_population = @program.program_populations.build

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @program }
    end
  end

  # GET /programs/1/edit
  def edit
  end
  
  private
    def get_camp
      @camp = Camp.find_by_uri(params[:camp_id])
      if !@camp
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_400 
      end
    end
    
    def get_camp_program
      @program = @camp.programs.find(params[:id])
    end
end
