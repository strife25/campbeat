class Camp::Campers::SessionsController < AuthorizedController
  skip_load_and_authorize_resource
  before_filter :get_camp
  authorize_resource :camp
  before_filter :get_camper
  authorize_resource :camper
  before_filter :get_session, :only => [:show, :update, :destroy]
  authorize_resource :session 
  
  
  # == GET /camps/{camp_uri}/campers/{camper_id}/sessions/{session_id}
  # 
  # Returns a list of all the campers attending the camp referenced by {camp_uri}
  #
  # === Response Codes:
  #
  # * 200 OK
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML Response:
  #
  #    <camper>
  #        <camper-uri>https://www.vertizu.com/campers/fhs48htos4h</camper-uri>
  #        <session-uri>https://www.vertizu.com/sessons/djw39rh48</session-uri>
  #        <program-uri>https://www.vertizu.com/programs/fj4ih48w</program-ur>
  #        <cabin-uri>https://www.vertizu.com/cabins/ajf8fus</cabin-uri>
  #        <schedule-uri>https://www.vertizu.com/campers/fhs48htos4h/sessions/djw39rh48/schedule</schedule-uri>
  #    </camper>
  #
  def show
    ca = CamperAttendance.where("camper_id = ? AND session_id = ?", @camper.id, @session.id).limit(1)[0]
    @program = ca.program
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml #show.xml.builder
      format.json { render :template => "camp/campers/sessions/show.json" }
    end
  end

  # == PUT /camps/{camp_uri}/campers/{camper_id}/sessions/{session_id}
  #
  # Update the program that the camper is attending.
  #
  # === Response Codes:
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO* 
  # * <tt>program-id {String}</tt> - The id of the program the camper will attend
  # * <tt>cabin-id {String}</tt> - The id of the cabin the camper will stay in
  #
  # === Example XML Response:
  #
  #    <camper>
  #        <program-uri>https://www.vertizu.com/programs/fj4ih48w</program-ur>
  #        <cabin-uri>https://www.vertizu.com/cabins/ajf8fus</cabin-uri>
  #    </camper>
  #
  def update
    if params[:program_id].nil? or !@camp.programs.exists?(params[:program_id])
      respond_to do |format|
        format.html { render :action => "edit", :status => :bad_request }
        format.xml  { render :xml => @session.errors, :status => :bad_request }
      end
    else
      program = @camp.programs.find(params[:program_id])
      if program.is_valid_age(@camper.age)
        ca = CamperAttendance.where("camper_id = ? AND session_id = ?", @camper.id, @session.id).limit(1)[0]

        program_pop = ProgramPopulation.where("program_id = ? AND session_id = ?", ca.program_id, @session.id).limit(1)[0]
        program_pop.decrement!(:current_population)
      
        ca.program_id = params[:program_id]
        program_pop = ProgramPopulation.where("program_id = ? AND session_id = ?", params[:program_id], @session.id).limit(1)[0]
        
        if program_pop
          program_pop.increment!(:current_population)
          isSuccess = ca.save!

          if isSuccess
            respond_to do |format|
              format.html { render :text => "you have updated the registered camper", :status => :accepted }
              format.xml  { head :accepted }
            end
          else
            respond_to do |format|
                format.html { render :action => "edit", :status => :internal_server_error }
                format.xml {render :xml => @session, :status => :internal_server_error, :location => @camper}
            end
          end
        else
          respond_to do |format|
              format.html { render :action => "edit", :status => :bad_request }
              format.xml {render :xml => @camper, :status => :bad_request, :location => @session}
          end
        end
      else
        respond_to do |format|
            format.html { render :action => "edit", :status => :bad_request }
            format.xml {render :xml => @camper, :status => :bad_request, :location => @session}
        end
      end
    end
  end

  # == DELETE /camps/{camp_id}/contacts/{id}
  #
  # Remove the camper from attending a given session
  #
  # === Response Codes: *TODO*
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  def destroy
    ca = CamperAttendance.where("camper_id = ? AND session_id = ?", @camper.id, @session.id).limit(1)[0]
    
    program_pop = ProgramPopulation.where("program_id = ? AND session_id = ?", ca.program_id, @session.id).limit(1)[0]
    program_pop.decrement!(:current_population)
    
    ca.destroy
    
    if ca.destroyed?
      respond_to do |format|
        format.html { render :text => "you have updated the registered camper", :status => :accepted }
        format.xml  { head :accepted }
      end
    end
  end
  
  private
    def get_camp
      @camp = Camp.find_by_uri(params[:camp_id])
      if !@camp
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_404
      end
    end
    
    def get_camper
      @camper = @camp.campers.find(params[:camper_id])
      if !@camper
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_404
      end
    end
    
    def get_session
      @session = @camper.sessions.find(params[:id])
      if !@session
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_404
      end
    end
end
