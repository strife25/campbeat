class Camp::ContactsController < AuthorizedController
  skip_load_and_authorize_resource
  before_filter :get_camp
  before_filter :get_camp_contact, :only => [:show, :edit, :update, :destroy]
  authorize_resource :camp
  load_and_authorize_resource :contact, :through => :camp
  
  # == GET /camps/{camp_id}/contacts
  #
  # Returns a list of all of a camp's contacts stored in the database. 
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 401 Unauthorized
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML Response:
  #
  #  <contacts>
  #    <contact>
  #      <first_name>Tim</first-name> 
  #      <middle_name>J</middle-name> 
  #      <last_name>Tutt</last-name> 
  #      <suffix>Jr.</suffix> 
  #      <email_address>ttutt@vt.edu</email-address>
  #      <day_phone>12345678901</day_phone>
  #      <evening_phone>12345678901</evening_phone>
  #      <mobile_phone>12345678901</mobile_phone>
  #    </contact>
  #
  #    <contact>
  #      <first_name>Tim</first-name> 
  #      <middle_name>J</middle-name> 
  #      <last_name>Tutt</last-name> 
  #      <suffix>Jr.</suffix> 
  #      <email_address>ttutt@vt.edu</email-address>
  #      <day_phone>12345678901</day_phone>
  #      <evening_phone>12345678901</evening_phone>
  #      <mobile_phone>12345678901</mobile_phone>
  #    </contact>
  #  </contacts>
  #
  def index
    @contacts = @camp.contacts.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml
      format.json { render :template => "camp/contacts/index.json" }
    end
  end
  
  # == POST /camps/{camp_id}/contacts
  #
  # Create a new contact resource for a camp.
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Required*: *TODO*
  # * <tt>first_name {String}</tt> - The first name of the contact.
  # * <tt>middle_name {String}</tt> - The middle name of the contact.
  # * <tt>last_name {String}</tt> - The last name of the contact.
  # * <tt>suffix {String}</tt> - The siffix name of the contact.
  # * <tt>email_address {String}</tt> - The email address of the contact.
  # * <tt>phone_number {String}</tt> - The phone number of the contact.
  #
  # === Example XML request:
  #
  #  <contact>
  #    <first-name>Tim</first-name> 
  #    <middle-name>J</middle-name> 
  #    <last-name>Tutt</last-name> 
  #    <suffix>Jr.</suffix> 
  #    <email-address>ttutt@vt.edu</email-address>
  #    <phone-number>12345678901</phone-number>
  #  </contact>
  def create
    @contact = @camp.contacts.build(params[:contact])
    
    respond_to do |format|
      if @contact.save
        format.html { render :text => "contact created!", :status => :created }
        format.xml {render :xml => @contact, :status => :created, :location => @contact}
      else
        format.html { render :action => "new", :status => :bad_request }
        format.xml  { render :xml => @contact.errors, :status => :bad_request }
      end
    end
  end

  # == GET /camps/{camp_id}/contacts/{id}
  #
  # Manages the retrieval of a single contact resource.
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  # * 404 Not Found
  #
  # === Supported Content Types: *TODO*
  #
  # * XML
  # * JSON
  #
  # === Example XML response:
  #
  #  <contact>
  #    <first_name>Tim</first_name> 
  #    <middle_name>J</middle_name> 
  #    <last_name>Tutt</last_name> 
  #    <suffix>Jr.</suffix> 
  #    <email_address>ttutt@vt.edu</email_address>
  #    <day_phone>12345678901</day_phone>
  #    <evening_phone>12345678901</evening_phone>
  #    <mobile_phone>12345678901</mobile_phone>
  #  </contact>
  #
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  
      format.json { render :template => "camp/contacts/show.json" }
    end
  end

  # == PUT /camps/{camp_id}/contacts/{id}
  # 
  # Update the data of a contact resource
  #
  # === Response Codes: *TODO*
  #
  # * 201 Created
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  # === Supported Content Types:
  #
  # * TODO: XML
  # * TODO: JSON
  #
  # === Parameters:
  #
  # *Optional*:
  # * <tt>first_name {String}</tt> - The first name of the contact.
  # * <tt>middle_name {String}</tt> - The middle name of the contact.
  # * <tt>last_name {String}</tt> - The last name of the contact.
  # * <tt>suffix {String}</tt> - The siffix name of the contact.
  # * <tt>email_address {String}</tt> - The email address of the contact.
  # * <tt>phone_number {String}</tt> - The phone number of the contact.
  #
  # === Example XML request:
  #
  #  <contact>
  #    <first-name>Tim</first-name> 
  #    <middle-name>J</middle-name> 
  #    <last-name>Tutt</last-name> 
  #    <suffix>Jr.</suffix> 
  #    <email-address>ttutt@vt.edu</email-address>
  #    <phone-number>12345678901</phone-number>
  #  </contact>
  #
  def update
    respond_to do |format|
      if @contact.update_attributes(params[:contact])
        format.html { render :text => "you have updated the contact model", :status => :accepted }
        format.xml  { head :accepted }
      else
        format.html { render :action => "edit", :status => :bad_request }
        format.xml  { render :xml => @contact.errors, :status => :bad_request }
      end
    end
  end

  # == DELETE /camps/{camp_id}/contacts/{id}
  #
  # Delete a contact resource
  #
  # === Response Codes: *TODO*
  #
  # * 202 Accepted
  # * 400 Bad Request
  # * 401 Unauthorized
  #
  def destroy
    @contact.destroy

    respond_to do |format|
      format.html { render :text => "contact destroyed!", :status => :accepted }
      format.xml  { head :accepted }
    end
  end
  
  def new
    @contact =  @camp.contacts.build
  end

  def edit
    @camp = Camp.find_by_uri(params[:camp_id])
    @contact = @camp.contacts.find(params[:id])
  end
  
  private
    def get_camp
      @camp = Camp.find_by_uri(params[:camp_id])
      if !@camp
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_400 
      end
    end
    
    def get_camp_contact
      @contact = @camp.contacts.find(params[:id])
      if !@contact
        #call parent 404 method because no record was found
        #ActiveRecord::RecordNotFound is not being called
        self.render_400 
      end
    end

end
