xml.instruct!
xml.camper do
  xml.camper_id @camper.id
  xml.program_id @program.id
  xml.session_id @session.id
end