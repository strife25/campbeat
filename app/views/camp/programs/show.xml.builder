xml.instruct!
xml.program do
  xml.id @program.id
  xml.camp_uri @program.camp.uri
  xml.name @program.name
  xml.description @program.description
  xml.min_age @program.min_age
  xml.max_age @program.max_age
  xml.price @program.price
  xml.sessions do
    @program.program_populations.each do |programPop|
      xml.session_id programPop.session_id
      xml.current_population programPop.current_population
      xml.max_population programPop.max_population
    end
  end
end