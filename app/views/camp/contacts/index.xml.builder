xml.instruct!
xml.contacts do
  @contacts.each do |contact|
    xml.contact do
      xml.id contact.id
      xml.first_name contact.first_name
      xml.middle_name contact.middle_name
      xml.last_name contact.last_name
      xml.suffix contact.suffix
      xml.email_address contact.email_address
      xml.day_phone contact.day_phone
      xml.evening_phone contact.evening_phone
      xml.mobile_phone contact.mobile_phone
    end
  end
end