xml.instruct!
xml.session do
  xml.id @session.id
  xml.camp_uri @session.camp.uri
  xml.start_date @session.start_date
  xml.end_date @session.end_date
  xml.programs do
    @session.programs.each do |program|
      xml.program_id program.id
    end
  end
end