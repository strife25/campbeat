xml.instruct!
xml.campers do
  @campers.each do |camper|
    xml.camper do
      xml.id camper.id
      xml.first_name camper.first_name
      xml.middle_name camper.middle_name
      xml.last_name camper.last_name
      xml.suffix camper.suffix
      xml.age camper.age
      xml.birthday camper.birthday
      xml.grade camper.grade
      xml.gender camper.gender
      xml.camps do
        camper.camps.each do |camp|
          xml.camp_uri camp.uri
        end
      end
      xml.guardians do
        camper.guardians.each do |guardian|
          xml.guardian do
    	      xml.first_name guardian.first_name
    	      xml.middle_name guardian.middle_name
    	      xml.last_name guardian.last_name
    	      xml.suffix guardian.suffix
    	      xml.email_address guardian.email_address
    	      xml.day_phone guardian.day_phone
    	      xml.evening_phone guardian.evening_phone
    	      xml.mobile_phone guardian.mobile_phone
    	    end
        end
      end
      xml.address do
    		xml.street_1 camper.address.street_1
    		xml.street_2 camper.address.street_2
    		xml.city camper.address.city
    		xml.state camper.address.state
    		xml.zip_code camper.address.zip_code
    		xml.country camper.address.country
    	end
    end
  end
end