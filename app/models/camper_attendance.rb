class CamperAttendance < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible :camper_id, :program_id, :session_id

  # Associations
  # ============
  belongs_to :camper
  belongs_to :program
  belongs_to :session
  
end
