class Session < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible  :camp_id, :start_date, :end_date

  # Associations
  # ============
  belongs_to :camp
  
  has_many :camper_attendances
  has_many :campers, :through => :camper_attendances
  has_many :program_populations
  has_many :programs, :through => :program_populations
  
  # Validations
  # ===========
  validates :camp, :presence => true
  validates :start_date, :presence => true, :timeliness => {:type => :date}
  validates :end_date, :presence => true, :timeliness => {:type => :date}
  
  validates_date :start_date, :before => :end_date, :if => :is_start_date_not_nil?
  validates_date :end_date, :after => :start_date, :if => :is_end_date_not_nil?
  
  # Public Methods
  # ==============

  # Return all of the programs for a given camp session that have vacancies.
  #
  scope :vacant, lambda {
    joins(:program_populations).
    where('program_populations.current_population < program_populations.max_population').
    group("sessions.id")
  }

  # Return all of the programs for a given camp session that are full.
  #
  scope :closed, lambda {
    joins(:program_populations).
    where('program_populations.current_population >= program_populations.max_population').
    group("sessions.id")
  }
  
  # Check whether or not the start_date is nil
  #
  def is_start_date_not_nil?
    self.start_date != nil
  end
  
  # Check whether or not the end_date is nil
  #
  def is_end_date_not_nil?
    self.end_date != nil
  end
end
