class Program < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible  :camp_id, :min_age, :name, :price

  # Associations
  # ============
  belongs_to :camp
  
  has_many :campers, :through => :camper_attendances
  has_many :camper_attendances
  has_many :program_populations, :dependent => :destroy
  has_many :sessions, :through => :program_populations
  
  accepts_nested_attributes_for :program_populations
  
  # Validations
  # ===========
  validates :camp, :presence => true
  validates :min_age, :presence => true, :numericality => {:only_integer => true}
  validates :name, :presence => true
  validates :price, :presence => true, :numericality => true 
  
  # Public Methods
  # ==============

  # Return all of the sessions for a given camp program that have vacancies.
  #
  scope :vacant, lambda {
    joins(:program_populations).
    where('program_populations.current_population < program_populations.max_population').
    group("programs.id")
  }

  # Return all of the sessions for a given camp program that are full.
  #
  scope :closed, lambda {
    joins(:program_populations).
    where('program_populations.current_population >= program_populations.max_population').
    group("programs.id")
  }
  
  # Returns whether or not a camper's age is within the acceptable range of the
  # program.
  #
  def is_valid_age(age)
    return ( age >= self.min_age and age <= self.max_age)
  end
end
