class ZipcodeFormatValidator < ActiveModel::EachValidator

  # Validate that the zip code value for a given record is of the 
  # correct format.
  #
  def validate_each(record,attribute,value)
    unless value =~ /^\d{5}$/ or value =~ /^\d{5}-\d{4}$/
      record.errors[attribute] << (options[:message] || "is not formatted properly")  
    end
  end
end