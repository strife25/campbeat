class User < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible :email, :password, :password_confirmation, :remember_me

  # Associations
  # ============
  has_many :campers

  has_and_belongs_to_many :camps
  has_and_belongs_to_many :roles
  
  # Devise Settings
  # ===============
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  
  # Public Methods
  # ==============
  
  def cb_admin?
    return self.roles.exists?(:name => "CbAdmin")
  end
  
  def camp_customer?
    return self.roles.exists?(:name => "CampCustomer")
  end
  
  def role?(role)
      return !!self.roles.find_by_name(role.to_s.camelize)
  end
  
  def is_linked_to_camp?(camp)
    self.camps.exists?(camp.id)
  end
  
  def is_obj_linked_to_camp?(obj)
    if obj.respond_to?(:camp)
      return self.camps.exists?(obj.camp.id)
    end
    
    return false
  end
  
  def is_contact_linked_to_camp?(contact)
    if contact.respond_to?(:contactable)
      return self.camps.exists?(contact.contactable.id)
    end
    
    return false
  end
  
  def is_linked_to_camper?(camper)
    self.campers.exists?(camper.id)
  end
end
