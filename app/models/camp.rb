class Camp < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible :address_attributes, :email_address, :name , :uri

  # Associations
  # ============
  has_many :contacts, :as => :contactable
  has_many :programs
  has_many :sessions
  
  has_and_belongs_to_many :campers
  has_and_belongs_to_many :users

  has_one :address, :as => :addressable, :dependent => :destroy
  accepts_nested_attributes_for :address, :allow_destroy => true
  
  # Validations
  # ===========
  validates :email_address, :email => true, :allow_nil => true
  validates :name, :presence => true 
  validates :uri, :presence => true, :uniqueness => true
  #validates :address, :associated => true
  
  # Public Methods
  # ==============

  # overriding this method makes sure that rails uses the URI attribute of a 
  # camp instead of its id when creating url paths.
  #
  def to_param
    uri
  end
end
