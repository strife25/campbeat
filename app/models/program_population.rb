class ProgramPopulation < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible :current_population, :max_population, :program_id, :session_id

  # Associations
  # ============
  belongs_to :program
  belongs_to :session
  
  # Validations
  # ===========
  validates :current_population, :numericality => {:only_integer => true}
  validate :current_population_is_not_greater_than_max_population

  validates :max_population, :presence => true, 
            :numericality => {:only_integer => true} 
  validate :max_population_is_greater_than_zero

  validates :program, :presence => true
  validates :session, :presence => true

  # a program can only be associated with one instance of each session
  validates :session_id, :uniqueness => {:scope => :program_id} 
  
  
  # Public Methods
  # ==============

  # Return a hash representing the JSON value of this model.
  #
  def as_json(options={})
    {
      :session_id => self.session_id,
      :current_popualtion => self.current_population,
      :max_population => self.max_population  
    }
  end
  
  # Determine if the program is open for more campers to attend.
  #
  def is_open
    if ( self.current_population and self.max_population \
         and self.current_population < self.max_population )
    then
      return true
    end
    
    return false
  end
  
  # Validate that the provided max_population is greater than zero
  #
  def max_population_is_greater_than_zero
    if ( self.max_population and self.max_population <= 0 ) then
      errors.add(:max_population, "must be greater than zero.")
    end
      
  end
  
  # Validate that current_population is not greater that 
  # the max_population attribute
  #
  def current_population_is_not_greater_than_max_population
    if ( self.current_population and self.max_population \
         and self.current_population > self.max_population )
    then
      errors.add(
        :current_population, 
        "can't be greater than the max population of a program."
      )
    end
      
  end
end
