class Address < ActiveRecord::Base

  # Relationships
  # =============
  belongs_to :addressable, :polymorphic => true
  
  # Mass Assignment Whitelist
  # =========================
  attr_accessible :street_1, :street_2, :city, :state, :zip_code, :country

  # Validations
  # ===========
  validates :street_1, :presence => true
  validates :city, :presence => true
  validates :state, :presence => true, :length => { :minimum => 2 }
  validates :zip_code, :presence => true, :zipcode_format => true, 
            :length => { :minimum => 5, :maximum => 10 }  
  validates :country, :presence => true 

end
