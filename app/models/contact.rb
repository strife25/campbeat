class Contact < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible :first_name, :middle_name, :last_name, :suffix, :email_address,
                  :day_phone, :evening_phone, :mobile_phone 

  # Associations
  # ============
  belongs_to :contactable, :polymorphic => true
  
  # Validations
  # =========== 
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :email_address, :presence => true, :email => true
  validates :day_phone, :presence => true, :phonenumber_format => true
  validates :evening_phone, :phonenumber_format => true
  validates :mobile_phone, :phonenumber_format => true
  
end
