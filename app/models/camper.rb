class Camper < ActiveRecord::Base

  # Mass Assignment Whitelist
  # =========================
  attr_accessible :first_name, :middle_name, :last_name, :suffix, :gender, 
                  :birthday, :grade, :address_attributes

  # Associations
  # ============
  belongs_to :user
  
  has_one :address, :as => :addressable

  has_many :camper_attendances
  has_many :guardians, :as => :contactable, :class_name => "Contact"  
  has_many :programs, :through => :camper_attendances
  has_many :sessions, :through => :camper_attendances
  
  has_and_belongs_to_many :camps

  accepts_nested_attributes_for :address, :allow_destroy => true
  accepts_nested_attributes_for :guardians, :allow_destroy => true

  # Validations
  # ===========  
  validates :first_name, :presence => true 
  validates :last_name, :presence => true 
  validates :birthday, :presence => true, :timeliness => {:type => :date} 
  validates :gender, :presence => true, :length => { :maximum => 1 }
  validates_inclusion_of :gender, :in => %w(M F m f) 
  
  # Public Methods
  # ==============

  # Get the age of the camper. This method accounts for leap years.
  #
  def age
    now = Time.now.utc.to_date
    now.year - self.birthday.year - ((now.month > self.birthday.month || (now.month == self.birthday.month && now.day >= self.birthday.day)) ? 0 : 1)
  end

end
