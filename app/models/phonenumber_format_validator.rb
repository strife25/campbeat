class PhonenumberFormatValidator < ActiveModel::EachValidator

  # Validate that the value for a phone number record is a 10 digit number.
  #
  def validate_each(record,attribute,value)
    unless value =~ /^\d{10}$/
      record.errors[attribute] << (options[:message] || "is not formatted properly")  
    end
  end
end