require 'test_helper'

class ProgramPopulationTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "create a valid program population" do
    pop = ProgramPopulation.create(
      :program_id => programs(:horse_camp).id,
      :session_id => sessions(:session_4).id,
      :max_population => 10  
    )
    
    assert_equal pop.current_population, 0, "current_population should default to 0"
    assert pop.valid?, "#{pop.errors.full_messages}"
  end
  
  test "create a program population without a program" do
    pop = ProgramPopulation.new(
      :session_id => sessions(:session_1).id,
      :current_population => 0,
      :max_population => 10  
    )
    
    assert !pop.valid?, "population object is valid without a program association"
  end
  
  test "create a program population without a session" do
    pop = ProgramPopulation.new(
      :program_id => programs(:horse_camp).id,
      :current_population => 0,
      :max_population => 10  
    )
    
    assert !pop.valid?, "population object is valid without a session association"
  end
  
  test "create a program population with an invalid current_population" do
    pop = ProgramPopulation.new(
      :program_id => programs(:horse_camp).id,
      :session_id => sessions(:session_1).id,
      :max_population => 10  
    )
    
    pop.current_population = "a"
    assert !pop.valid?, "population object is considered valid with an invalid current_population"
    
    pop.current_population = 11
    assert !pop.valid?, "population object is considered valid with a current_population that is greater than the max_population"
  end
  
  test "create a program population without a max_population and an invalid max_population" do
    pop = ProgramPopulation.new(
      :program_id => programs(:horse_camp).id,
      :session_id => sessions(:session_1).id,
      :current_population => 0
    )
    
    assert !pop.valid?, "population object is considered valid without a max_population"
    
    pop.max_population = "a"
    assert !pop.valid?, "population object is considered valid with an invalid max_population"
  end
  
  test "session_id must be unique in the scope of a program_id" do
    pop = ProgramPopulation.new(
      :program_id => programs(:traditional).id,
      :session_id => sessions(:session_1).id,
      :current_population => 0,
      :max_population => 10  
    )
    
    assert !pop.valid?, "population object is valid with a program that does not have a unique sesion"
  end
end
