require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  test "save a new address" do
    camps(:test_camp).build_address(
      :street_1 => "501 Dehart St.", \
      :city => "blacksburg", \
      :state => "MA", \
      :zip_code => "12345", \
      :country => "USA"
    )
    
    assert camps(:test_camp).address.valid?, "Address model is not in a valid state"
  end
  
  test "new address should have street_1" do
    camps(:test_camp).build_address(
      :city => "blacksburg", \
      :state => "MA", \
      :zip_code => "12345", \
      :country => "USA"
    )
    
    assert !camps(:test_camp).address.valid?, "Address model is valid without a street_1 attribute"
  end
  
  test "new address should have a city" do
    camps(:test_camp).build_address(
      :street_1 => "501 Dehart St.", \
      :state => "MA", \
      :zip_code => "12345", \
      :country => "USA"
    )
    
    assert !camps(:test_camp).address.valid?, "Address model is valid without a city"
  end
  
  test "new address should have a state in valid format" do
    camps(:test_camp).build_address(
      :street_1 => "501 Dehart St.", \
      :city => "blacksburg", \
      :zip_code => "12345", \
      :country => "USA"
    )
    
    assert !camps(:test_camp).address.valid?, "Address model is valid without a state"
    
    camps(:test_camp).address.state = "M"
    assert !camps(:test_camp).address.valid?, "Address model is valid with a bad state string"
  end
  
  test "new address should have a zip code in valid format" do
    camps(:test_camp).build_address(
      :street_1 => "501 Dehart St.", \
      :city => "blacksburg", \
      :state => "MA", \
      :country => "USA"
    )
    
    assert !camps(:test_camp).address.valid?, "Address model is valid without a zip code"
    
    camps(:test_camp).address.zip_code = "1234"
    assert !camps(:test_camp).address.valid?, "Address model is valid with a short zip code"
    
    camps(:test_camp).address.zip_code = "1234567890"
    assert !camps(:test_camp).address.valid?, "Address model is valid with incompliant zip code format"
    
    camps(:test_camp).address.zip_code = "12345"
    assert camps(:test_camp).address.valid?, "Address model is invalid with correct zip code format"
    
    camps(:test_camp).address.zip_code = "12345-6789"
    assert camps(:test_camp).address.valid?, "Address model is invalid with correct zip code format"
  end
  
  test "new address should have a country" do
    camps(:test_camp).build_address(
      :street_1 => "501 Dehart St.", \
      :city => "blacksburg", \
      :state => "MA", \
      :zip_code => "12345"
    )
    
    assert !camps(:test_camp).address.valid?, "Address model is valid without a country"
  end
end
