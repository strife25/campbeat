require 'test_helper'

class CampTest < ActiveSupport::TestCase

  test "create a valid camp" do
    camp = Camp.new(
      :uri => "testSave", \
      :name => "Camp Test"
    )
    
    assert camp.valid?, "Camp object is not in a valid format"
  end
  
  test "create a valid camp with a proper email address" do
    camp = Camp.new(
      :uri => "testSave", \
      :name => "Camp Test", \
      :email_address => "test@test.com"
    )
    
    assert camp.valid?, "Camp object with an email is not valid"
    
    camp.email_address = "t"
    assert !camp.valid?, "Camp object with an invalid email is not valid"
  end
  
  test "camp is not valid without a unique uri" do
    camp = Camp.new(
      :name => "Camp Test"
    )

    assert !camp.valid?, "New camp was valid without a uri"
    
    camp.uri = camps(:bolo).uri
    assert !camp.valid?, "New camp was valid with a unique uri"
  end
  
  test "new camp should not be saved without a name" do
    camp = Camp.new(
      :uri => "test"
    )

    assert !camp.valid?, "New camp was valid without a name."
  end
  
  test "save a camp with an address" do
    camp = Camp.new(
      :uri => "testSave", \
      :name => "Camp Test",
      :address_attributes => {
          :street_1 => "Test rd.", \
          :city => "blacksburg", \
          :state => "MA", \
          :zip_code => "01005", \
          :country => "USA"
      } 
    )
    assert camp.save
    assert camp.address.valid?
    
    
    assert camps(:test_camp).build_address(
      :street_1 => "Test rd.", \
      :city => "blacksburg", \
      :state => "MA", \
      :zip_code => "01005", \
      :country => "USA"
    ), "Unable to save the new camp resource with an address."
    
    assert camps(:test_camp).address.valid?, "test_camp's address is not valid"
    assert_equal camps(:test_camp).address.street_1, "Test rd.", "The saved camp does not contain the value 'Test rd.' in its address"
  end
  
  test "verify that the uri is the primary id to access the camp resource" do
    assert_equal camps(:bolo).to_param, "bolo", "The uri is not used as the primary id"
  end
  
  test "camp has many campers" do
    assert_equal \
      camps(:bolo).campers.find(campers(:one).id).first_name, \
      campers(:one).first_name, \
      "the first camper in the camp is not equal to camper(:one).first_name"
  end
  
  test "camp has many contacts" do
    john_id = contacts(:contact_john).id
    bob_id = contacts(:contact_bob).id
    
    assert_equal camps(:bolo).contacts.find(john_id).first_name, contacts(:contact_john).first_name, "Camp's contact is not named john!"
    assert_equal camps(:bolo).contacts.find(bob_id).first_name, contacts(:contact_bob).first_name, "Camp's contact is not named bob!"
  end
end
