require 'test_helper'

class SessionTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "create a valid session object" do
    session = Session.new(
      :camp_id => camps(:bolo).id,
      :start_date => 2.days.ago,
      :end_date => 2.days.from_now 
    )
    
    assert session.valid?, "New session object is not considered valid"
  end
  
  test "create a session object without a camp" do
    session = Session.new(
      :start_date => 2.days.ago,
      :end_date => 2.days.from_now 
    )
    
    assert !session.valid?, "New session object is valid without a camp"
  end
  
  test "create a session object without a start_date and invalid start_date" do
    session = Session.new(
      :camp_id => camps(:bolo).id,
      :end_date => 2.days.from_now 
    )
    
    assert !session.valid?, "new session is valid without a start_date"
    
    session.start_date = "abcd"
    assert !session.valid?, "new session is valid with an invalid start_date format"
  end
  
  test "create a session object without a end_date and invalid end_date" do
    session = Session.new(
      :camp_id => camps(:bolo).id,
      :start_date => 2.days.ago
    )
    
    assert !session.valid?, "new session is valid without a end_date"
    
    session.end_date = "abcd"
    assert !session.valid?, "new session is valid with an invalid end_date format"
  end
  
  test "create a session with a start date set after the end date" do
    session = Session.new(
      :camp_id => camps(:bolo).id,
      :start_date => 3.days.from_now,
      :end_date => 2.days.from_now 
    )
    
    assert !session.valid?, "New session object is valid with a start date set after the end date"
  end
  
  test "create a session with an end date set before the start date" do
    session = Session.new(
      :camp_id => camps(:bolo).id,
      :start_date => 2.days.ago,
      :end_date => 3.days.ago
    )
    
    assert !session.valid?, "New session object is valid with an end date set before the start date"
  end
  
  test "session has many programs" do
    assert_equal sessions(:session_1).programs.first.name, "Traditional Camp", "session_1's first program is not named 'Traditional Camp'"
  end
  
  test "session has many program_populations" do
    assert_equal sessions(:session_1).program_populations.find(program_populations(:one).id).max_population, 10, "session_1's program max population for traditional camp is not equal to 10" 
  end
  
  test "session has many campers" do
    assert_equal \
      sessions(:session_1).campers.find(campers(:one).id).first_name, \
      campers(:one).first_name, \
      "the first camper in the traditional program is not equal to camper(:one).first_name"
  end
end
