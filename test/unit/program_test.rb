require 'test_helper'

class ProgramTest < ActiveSupport::TestCase
  
  test "create a valid program object" do
    program = Program.new(
      :camp_id => camps(:bolo).id,
      :name => "Test Program", 
      :min_age => 10, 
      :price => 365.00   
    )
    
    assert program.valid?, "new program object is not valid"
  end
  
  test "create a program object with no camp" do
    program = Program.new(
      :name => "Test Program", 
      :min_age => 10, 
      :price => 365.00   
    )
    
    assert !program.valid?, "new program object is valid without a camp"
  end
  
  test "create a program object with no name" do
    program = Program.new(
      :camp_id => camps(:bolo),
      :min_age => 10,
      :price => 365.00   
    )
    
    assert !program.valid?, "new program object is valid without a name"
  end
  
  test "create a program object with no min_age and invalid age" do
    program = Program.new(
      :camp_id => camps(:bolo),
      :name => "Test Program",
      :price => 365.00   
    )
    
    assert !program.valid?, "new program object is valid without a min_age"
    
    program.min_age = "a"
    assert !program.valid?, "new program object is valid with a min_age that is a string instead of int"
    
    program.min_age = 1.0
    assert !program.valid?, "new program object is valid with a min_age that is a decimal instead of int"
  end
  
  test "create a program object with no price and invalid price" do
    program = Program.new(
      :camp_id => camps(:bolo),
      :name => "Test Program",
      :min_age => 10  
    )
    
    assert !program.valid?, "new program object is valid without a price"
    
    program.price = "one dollar"
    assert !program.valid?, "new program object is valid with a price that is a string instead of decimal"
  end
  
  test "program has many sessions" do
    assert_equal \
      programs(:traditional).sessions.find(sessions(:session_1).id).start_date.to_s(:db), \
      1.week.ago.strftime("%Y-%m-%d"), \
      "the traditional program's session start date is not equal to 1 week ago"
  end
  
  test "program has many program populations" do
    assert_equal \
      programs(:traditional).program_populations.first.max_population, \
      10,
      "the first program population's max population is not equal to 20"
      
    assert_equal \
      programs(:traditional).program_populations.find(program_populations(:one).id).session_id, \
      sessions(:session_1).id,
      "the first program population's session is not equal to the session_1 fixture's id"
  end
  
  test "program has many campers" do
    assert_equal \
      programs(:traditional).campers.first.first_name, \
      campers(:one).first_name, \
      "the first camper in the traditional program is not equal to camper(:one).first_name"
  end
end
