require 'test_helper'

class CamperTest < ActiveSupport::TestCase
  test "create a valid camper object" do
    camper = Camper.new(
      :first_name => "John",
      :last_name => "Doe",
      :gender => "M",
      :birthday => 12.years.ago
    )
    
    assert camper.valid?, "#{camper.errors.full_messages}"
    
    camper.gender = "F"
    assert camper.valid?, "#{camper.errors.full_messages}"
    
    camper.gender = "m"
    assert camper.valid?, "#{camper.errors.full_messages}"
    
    camper.gender = "f"
    assert camper.valid?, "#{camper.errors.full_messages}"
    
  end
  
  test "create camper object without a first name" do
    camper = Camper.new(
      :last_name => "Doe",
      :gender => "M",
      :birthday => 12.years.ago
    )
    
    assert !camper.valid?, "camper object is valid w/o a first name!"
  end
  
  test "create camper object without a last name" do
    camper = Camper.new(
      :first_name => "John",
      :gender => "M",
      :birthday => 12.years.ago
    )
    
    assert !camper.valid?, "camper object is valid w/o a last name!"
  end
  
  test "create camper object without a gender or bad gender input" do
    camper = Camper.new(
      :first_name => "John",
      :last_name => "Doe",
      :birthday => 12.years.ago
    )
    
    assert !camper.valid?, "camper object is valid w/o a gender!"
    
    camper.gender = "Ma"
    assert !camper.valid?, "camper object is valid w/ too long of a gender"
    
    camper.gender = "C"
    assert !camper.valid?, "camper object is valid w/ an invalid gender value"
  end
  
  test "create camper object without a birthday or bad format" do
    camper = Camper.new(
      :first_name => "John",
      :last_name => "Doe",
      :gender => "M"
    )
    
    assert !camper.valid?, "camper object is valid w/o a birthday!"
    
    camper.birthday = "abc"
    assert !camper.valid?, "camper object is valid w/ a bad format for birthday!"
  end
  
  test "create a camper object that has an address" do
    camper = Camper.new(
      :first_name => "John",
      :last_name => "Doe",
      :gender => "M",
      :birthday => 12.years.ago,
      :address_attributes => {
          :street_1 => "Test rd.", \
          :city => "blacksburg", \
          :state => "MA", \
          :zip_code => "01005", \
          :country => "USA"
      }
    )
    
    assert camper.save, "#{camper.errors.full_messages}"
    assert camper.address.valid?, "#{camper.address.errors.full_messages}"
  end
  
  test "camper has many camps" do
    assert_equal campers(:one).camps.first.name, camps(:bolo).name, "Camper's camp is not Camp Bolo!"
  end
  
  test "camper has many guardians" do
    dad_id = contacts(:dad).id
    mom_id = contacts(:mom).id
    
    assert_equal campers(:one).guardians.find(dad_id).first_name, contacts(:dad).first_name, "Camper's father is not named john!"
    assert_equal campers(:one).guardians.find(mom_id).first_name, contacts(:mom).first_name, "Camper's mother is not named jane!"
  end

  test "camper has many programs" do
    assert_equal campers(:one).programs.first.name, programs(:traditional).name, "Camper's program is not traditional camp!"
  end
  
  test "camper has many sessions" do
    assert_equal \
      campers(:one).sessions.find(sessions(:session_1).id).start_date.to_s(:db), \
      sessions(:session_1).start_date.to_s(:db)
  end
end
