require 'test_helper'

class ContactTest < ActiveSupport::TestCase
  test "create a valid contact object" do
    contact = Contact.new(
      :first_name => "first", 
      :middle_name => "middle", 
      :last_name => "last", 
      :suffix => "III", 
      :email_address => "first@test.com", 
      :day_phone => "1234567890",
      :evening_phone => "1234567890",
      :mobile_phone => "1234567890"
    )
    
    assert contact.valid?, "contact object is not a valid type"
  end
  
  test "not able to create contact without first_name" do
    contact = Contact.new(
      :middle_name => "middle", 
      :last_name => "last", 
      :suffix => "III", 
      :email_address => "first@test.com", 
      :day_phone => "1234567890",
      :evening_phone => "1234567890",
      :mobile_phone => "1234567890"
    )
    
    assert !contact.valid?, "contact object valid without first_name"
  end
  
  test "not able to create contact without last_name" do
    contact = Contact.new(
      :first_name => "first", 
      :middle_name => "middle", 
      :suffix => "III", 
      :email_address => "first@test.com", 
      :day_phone => "1234567890",
      :evening_phone => "1234567890",
      :mobile_phone => "1234567890"
    )
    
    assert !contact.valid?, "contact object valid without last_name"
  end
  
  test "not able to create contact without a valid email" do
    contact = Contact.new(
      :first_name => "first", 
      :middle_name => "middle", 
      :last_name => "last", 
      :suffix => "III", 
      :day_phone => "1234567890",
      :evening_phone => "1234567890",
      :mobile_phone => "1234567890"
    )
    
    assert !contact.valid?, "contact object valid without email_address"
    
    contact.email_address = "t"
    assert !contact.valid?, "contact object valid with an invalid email address"
  end
  
  test "not able to create a valid contact object with an invalid day_phone" do
    contact = Contact.new(
      :first_name => "first", 
      :middle_name => "middle", 
      :last_name => "last", 
      :suffix => "III", 
      :email_address => "first@test.com", 
      :evening_phone => "1234567890",
      :mobile_phone => "1234567890"
    )
    
    assert !contact.valid?, "contact object is valid without a day_phone"
    
    contact.day_phone = "1"
    assert !contact.valid?, "contact object valid with an invalid phone number format"
  end
  
  test "not able to create a valid contact object with an invalid evening_phone" do
    contact = Contact.new(
      :first_name => "first", 
      :middle_name => "middle", 
      :last_name => "last", 
      :suffix => "III", 
      :email_address => "first@test.com", 
      :day_phone => "1234567890",
      :evening_phone => "1",
      :mobile_phone => "1234567890"
    )
    
    assert !contact.valid?, "contact object valid with an invalid phone number format"
  end
  
  test "not able to create a valid contact object with an invalid mobile_phone" do
    contact = Contact.new(
      :first_name => "first", 
      :middle_name => "middle", 
      :last_name => "last", 
      :suffix => "III", 
      :email_address => "first@test.com", 
      :day_phone => "1234567890",
      :evening_phone => "1234567890",
      :mobile_phone => "1"
    )
    
    assert !contact.valid?, "contact object valid with an invalid phone number format"
  end
end
