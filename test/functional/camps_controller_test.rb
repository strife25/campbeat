require 'test_helper'

class CampsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  def setup
    sign_in users(:dev)
  end
    
  #*****************
  # Test routes
  #*****************
  test "camps resource routes" do
    assert_routing '/camps', { :controller => "camps", :action => "index"  }
    assert_routing '/camps/bolo', { :controller => "camps", :action => "show", :id => "bolo" }
  end
  
  
  #*****************
  # Test /camps
  #*****************
  test "should get camps collection" do
    get :index
    assert_response :success
    assert_not_nil assigns(:camps)
  end
  
  test "only admin users can access camps collection" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :index
    assert_response :unauthorized
  end

  # test "should get new" do
  #   get :new
  #   assert_response :success
  # end

  test "should create camp" do
    assert_difference('Camp.count') do
      post :create, :camp => { 
        :name => "Camp Tester",
        :uri => "test-camp",
        :email_address => "test@test.com",
        :address_attributes => {
          :street_1 => "mccormick rd",
          :city => "barrer", 
          :state => "MA", 
          :zip_code => "01005", 
          :country => "USA" 
        }
      }
    end

    assert_response :created
    assert_not_nil assigns(:camp)
    
    get :show, :id => "test-camp"
    assert_response :success
  end
  
  test "create a camp with a bad request" do
    post :create, :camp => { 
      :name => "Camp Tester"
    }
    
    assert_response :bad_request
  end
  
  test "authorized user can create a camp" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    post :create, :camp => { 
      :name => "Camp Tester",
      :uri => "test-camp", 
    }
    assert_response :created
  end
  
  test "unauthorized access to create a camp" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    post :create, :camp => { 
      :name => "Camp Tester",
      :uri => "test-camp2", 
    }
    assert_response :unauthorized
    
  end

  #*****************
  # Test /camps/{id}
  #*****************
  test "should show camp" do
    get :show, :id => camps(:bolo).uri
    assert_response :success
  end
  
  test "unauthorized get request on a camp resource" do
    sign_in users(:parent)
    
    get :show, :id => camps(:bolo).uri
    assert_response :unauthorized
  end
  
  test "get with camp_admin user" do
    sign_in users(:boloAdmin)
        
    get :show, :id => camps(:bolo).uri
    assert_response :success
  end
  
  test "get request on unknown camp" do
    get :show, :id => "_"
    assert_response :missing
  end
  
  
  test "should update camp" do
    post :create, :camp => { 
      :name => "Camp Tester",
      :uri => "test-camp",
      :email_address => "test@test.com",
      :address_attributes => {
        :street_1 => "mccormick rd",
        :city => "barrer", 
        :state => "MA", 
        :zip_code => "01005", 
        :country => "USA" 
      }
    }
    
    put :update, :id => assigns(:camp).uri, :camp => {
      :name => assigns(:camp).name,
      :uri => assigns(:camp).uri,
      :email_address => "new@bolo.com"
    }
    
    assert_response :accepted
    assert_equal assigns(:camp).email_address, "new@bolo.com"
  end
  
  test "update a camp as a camp_admin user" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    post :create, :camp => { 
      :name => "Camp Tester",
      :uri => "test-camp",
      :email_address => "test@test.com",
      :address_attributes => {
        :street_1 => "mccormick rd",
        :city => "barrer", 
        :state => "MA", 
        :zip_code => "01005", 
        :country => "USA" 
      }
    }
    
    put :update, :id => assigns(:camp).uri, :camp => {
      :name => assigns(:camp).name,
      :uri => assigns(:camp).uri,
      :email_address => "new@bolo.com"
    }
    
    assert_response :accepted
    assert_equal assigns(:camp).email_address, "new@bolo.com"
  end
  
  test "update a camp as an unauthorized user" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    put :update, :id => camps(:bolo).uri, :camp => {
      :email_address => "hi@bolo.com"
    }
    
    assert_response :unauthorized
    assert_equal camps(:bolo).email_address, "hi@bolo.com"
  end
  
  test "update a camp with a bad request" do
    put :update, :id => camps(:bolo).to_param, :camp => {
      :blah => "blah"
    }
    
    assert_response :bad_request
  end

  test "update unknown camp" do
    put :update, :id => "_", :camp => {
      :email_address => "new@bolo.com"
    }
    
    assert_response :missing
  end
  
  test "should destroy camp" do
    assert_difference('Camp.count', -1) do
      delete :destroy, :id => camps(:test_camp).uri
    end

    assert_response :accepted
  end
  
  test "try to destroy camp with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    amt = Camp.count
    delete :destroy, :id => camps(:test_camp).uri 
    
    assert_response :unauthorized
    assert_equal amt, Camp.count
  end
  
  test "destroy a camp as a camp_admin user" do
    sign_in users(:boloAdmin)
    
    assert_difference('Camp.count', -1) do
      delete :destroy, :id => camps(:bolo).uri
    end

    assert_response :accepted
    
    delete :destroy, :id => camps(:test_camp).uri
    assert_response :unauthorized
  end
  
  test "delete unknown camp" do
    delete :destroy, :id => "_"
    assert_response :missing
  end
end
