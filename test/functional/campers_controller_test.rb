require 'test_helper'

class CampersControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  def setup
    sign_in users(:dev)
  end
  
  # Test routes
  # ===========
  test "campers resource routes" do
    assert_routing 'campers', { :controller => "campers", :action => "index"  }
    assert_routing '/campers/1', { :controller => "campers", :action => "show", :id => "1" }
  end
  
  # Test /campers
  # =============
  test "should get campers collection" do
    get :index
    
    assert_response :success
    assert_not_nil assigns(:campers)
  end
  
  test "only authorized users can access campers collection" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :index
    assert_response :unauthorized
  end
  
  test "a camp_customer user cannot access campers collection" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :index
    assert_response :unauthorized
  end
  
  test "should create camper" do
    assert_difference('Camper.count') do
      post :create, 
        :camper => {
          :first_name => "John",
          :last_name => "Doe",
          :gender => "M",
          :birthday => 12.years.ago  
        }
    end

    assert_response :created
    assert_not_nil assigns(:camper)

    get :show, :id => assigns(:camper).id
    assert_response :success
  end
  
  test "create a camper with a bad request" do
    #create a program without a name
    post :create, 
      :camper => {  }

    assert_response :bad_request
  end
  
  test "authorized user can create a camper" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    post :create, 
      :camper => {
        :first_name => "John",
        :last_name => "Doe",
        :gender => "M",
        :birthday => 12.years.ago  
      }
    assert_response :created
    
    get :show, :id => assigns(:camper).id
    assert_response :success
  end
  
  test "unauthorized access to create a camper" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    post :create, 
      :camper => {
        :first_name => "John",
        :last_name => "Doe",
        :gender => "M",
        :birthday => 12.years.ago  
      }
    assert_response :unauthorized
  end
  
  # Test /campes/{id}
  # =================
  test "should show camper" do
    get :show, :id => campers(:one).id
    assert_response :success
  end
  
  test "authorized get request for a camper" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :show, :id => campers(:one).to_param  
    assert_response :success
  end
  
  test "unauthorized get request on a camper resource" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :show, :id => campers(:one).to_param  
    assert_response :unauthorized
    
    # TODO: test that a camp admin can access campers that are a part of their camps
  end
  
  test "get request on unknown camper" do
    get :show, :id => "_"
    assert_response :missing
  end
  
  test "should update camper" do
    post :create, 
      :camper => {
        :first_name => "John",
        :last_name => "Doe",
        :gender => "M",
        :birthday => 2.days.ago  
      }

    put :update, 
      :id => assigns(:camper).to_param, 
      :camper => {
        :birthday => 20.years.ago  
      }

    assert_response :accepted
    assert_equal assigns(:camper).age, 20
  end
  
  test "update a camper with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    put :update, 
      :id => campers(:one).to_param, 
      :camper => {
        :birthday => 20.years.ago
      }

    assert_response :unauthorized
    assert_equal campers(:one).age, 12
    
    sign_out users(:boloAdmin)
    sign_in users(:parent)
    
   
  end
  
  test "authorized user can update a camper" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    post :create, 
      :camper => {
        :first_name => "John",
        :last_name => "Doe",
        :gender => "M",
        :birthday => 12.years.ago  
      }
    assert_response :created
    
     put :update, 
      :id => assigns(:camper).to_param, 
      :camper => {
        :birthday => 20.years.ago  
      }

    assert_response :accepted
    assert_equal assigns(:camper).age, 20
  end
  
  test "update a camper with a bad request" do
    put :update, 
      :id => campers(:one).to_param, 
      :camper => {
        :birthday => "a",
        :min_age => 80
      }
       
    assert_response :bad_request
  end
  
  test "update unknown camper" do
    put :update, :id => "_", 
      :camper => {
        :birthday => "a"
      }

    assert_response :missing
  end
  
  test "should destroy camper" do
    assert_difference('Camper.count', -1) do
      delete :destroy, :id => campers(:one).to_param
    end

    assert_response :accepted
  end

  test "try to destroy camper with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    amt = Camper.count
    delete :destroy, :id => campers(:one).to_param

    assert_response :unauthorized
    assert_equal amt, Camper.count
  end
  
  test "destroy a camper with authorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    amt = Camper.count-1 #ending amount
    delete :destroy, :id => campers(:one).to_param

    assert_response :accepted
    assert_equal amt, Camper.count
  end

  test "delete unknown camper" do
    delete :destroy, :id => "_"
    assert_response :missing
  end
end
