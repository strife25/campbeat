require 'test_helper'

class RegistrationsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  def setup
    sign_in users(:dev)
  end

  test "should get show" do
    get :show, :camp_id => camps(:bolo).uri
    assert_response :success
  end

end
