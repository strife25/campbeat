require 'test_helper'

class Camp::Campers::SessionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  def setup
    sign_in users(:dev)
  end
  
  #*****************
  # Test routes
  #*****************
  test "camp_campers_sessions resource routes" do
    show_uri = '/camps/bolo/campers/' + campers(:one).id.to_s  + '/sessions/' + sessions(:session_1).id.to_s
    assert_routing show_uri, { :controller => "camp/campers/sessions", :action => "show", :id => sessions(:session_1).id.to_s, :camp_id => "bolo", :camper_id => campers(:one).id.to_s }
  end
  
  #*****************
  # Test /camps/{camp_id}/campers/{camper_id}/sessions/{session_id}
  #*****************
  
  # GET
  # ===

  test "get camper session data" do
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
      
    assert_response :success
  end
  
  test "get with unknown camp" do
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => "_",
      :camper_id => campers(:one).id
      
      
    assert_response :not_found
  end
  
  test "get an unknown camper's session" do
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => "_"
      
    assert_response :not_found
  end
  
  test "get an unknown session for the camper" do
    get :show, 
      :id => "_",
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
    
    assert_response :not_found
  end
  
  test "authorized user gets a camper session" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
      
    assert_response :success
  end
  
  test "unauthorized camp_admin requests a camper session" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
      
    assert_response :unauthorized
  end
  
  test "invalid user requests the camper's session" do
    sign_out users(:dev)
    sign_in users(:parent )
    
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
      
    assert_response :unauthorized
  end
  
  # PUT
  # ===
  
  test "update the program the camper registered to" do
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id,
      :program_id => programs(:horse_camp).id
        
    assert_response :accepted
    assert_not_nil campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:horse_camp).id, sessions(:session_1).id)
    assert campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:traditional).id, sessions(:session_1).id).empty?
    assert_equal program_populations(:one).current_population, -1, "the population should be 0!"
    assert_equal program_populations(:two).current_population, 1, "the population should be 1!"
  end
  
  test "register camper to unknown program" do
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id,
      :program_id => programs(:tc_program).id
      
    assert_response :bad_request
  end
  
  test "register camper to program that is not linked to given session" do
    put :update, 
      :id => sessions(:session_2).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id,
      :program_id => programs(:test_program_camp).id
      
    assert_response :bad_request
  end
  
  test "register camper to program they are not the right age for" do
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:young).id,
      :program_id => programs(:traditional).id
      
    assert_response :bad_request
    
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:old).id,
      :program_id => programs(:traditional).id
      
    assert_response :bad_request
  end
  
  test "update the cabin the camper is staying in" do
    flunk("cabins not implemented yet!")
  end
  
  test "add camper to unknown cabin" do
    flunk("cabins not implemented yet!")
  end
  
  test "authorized user updates a camper session" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id,
      :program_id => programs(:horse_camp).id
        
    assert_response :accepted
    assert_not_nil campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:horse_camp).id, sessions(:session_1).id)
    assert campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:traditional).id, sessions(:session_1).id).empty?
    assert_equal program_populations(:one).current_population, -1, "the population should be 0!"
    assert_equal program_populations(:two).current_population, 1, "the population should be 1!"
  end
  
  test "unauthorized camp_admin requests to update a camper session" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id,
      :program_id => programs(:horse_camp).id
        
    assert_response :unauthorized
    assert_not_nil campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:traditional).id, sessions(:session_1).id)
    assert campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:horse_camp).id, sessions(:session_1).id).empty?
    assert_equal program_populations(:one).current_population, 0, "the population should be 0!"
    assert_equal program_populations(:two).current_population, 0, "the population should be 1!"
  end
  
  test "invalid user updates the camper's session" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id,
      :program_id => programs(:horse_camp).id
        
    assert_response :unauthorized
    assert_not_nil campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:traditional).id, sessions(:session_1).id)
    assert campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:horse_camp).id, sessions(:session_1).id).empty?
    assert_equal program_populations(:one).current_population, 0, "the population should be 0!"
    assert_equal program_populations(:two).current_population, 0, "the population should be 1!"
  end
  
  test "update with no program param" do
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
    assert_response :bad_request
  end
  
  test "update with unknown camp" do
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => "_",
      :camper_id => campers(:one).id,
      :program_id => programs(:horse_camp).id
      
    assert_response :not_found
  end
  
  test "update an unknown camper" do
    put :update, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => "_",
      :program_id => programs(:horse_camp).id
      
    assert_response :not_found
  end
  
  test "update an unknown session" do
    put :update, 
      :id => "_",
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id,
      :program_id => programs(:horse_camp).id
      
    assert_response :not_found
  end
  
  # DELETE
  # ======
  
  test "remove the camper from the session" do
    delete :destroy,
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
    assert_response :accepted
    assert campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:traditional).id, sessions(:session_1).id).empty?
    assert_equal program_populations(:one).current_population, -1, "the population should be 0!"
  end
  
  test "delete with unknown camp" do
    delete :destroy,
      :id => sessions(:session_1).to_param,
      :camp_id => "_",
      :camper_id => campers(:one).id
      
    assert_response :not_found
  end
  
  test "remove unknown camper from the session" do
    delete :destroy,
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => "_"
      
    assert_response :not_found
  end
  
  test "remove unknown session from a camper" do
    delete :destroy,
      :id => sessions(:session_3).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
    
    assert_response :not_found
  end
  
  test "authorized user removes a camper session" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    delete :destroy,
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
    assert_response :accepted
    assert campers(:one).camper_attendances.where('program_id =? AND session_id = ?', programs(:traditional).id, sessions(:session_1).id).empty?
    assert_equal program_populations(:one).current_population, -1, "the population should be 0!"
  end
  
  test "unauthorized camp_admin requests to remove a camper session" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    delete :destroy,
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
    assert_response :unauthorized
    assert_equal program_populations(:one).current_population, 0, "the population should be 0!"
  end 
  
  test "invalid user requests to deregister camper" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    delete :destroy,
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri,
      :camper_id => campers(:one).id
      
    assert_response :unauthorized
    assert_equal program_populations(:one).current_population, 0, "the population should be 0!"
  end

end
