require 'test_helper'

class Camp::CampersControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  def setup
    sign_in users(:dev)
  end
  
  #*****************
  # Test routes
  #*****************
  test "camp_campers resource routes" do
    assert_routing '/camps/bolo/campers', { :controller => "camp/campers", :action => "index", :camp_id => "bolo"  }
    assert_routing '/camps/bolo/campers/1', { :controller => "camp/campers", :action => "show", :id => "1", :camp_id => "bolo" }
  end
  
  #*****************
  # Test /camps/{camp_id}/campers
  #*****************
  test "get all campers registered to a camp" do
    get :index, :camp_id => camps(:bolo).uri
    
    assert_response :success
    assert_not_nil assigns(:campers)
  end
  
  test "get campers by age" do
    #assert min age query
    get :index, :camp_id => camps(:bolo).uri, :min_age => 8
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert camper.age >= 8
    end
    
    #assert max age query
    get :index, :camp_id => camps(:bolo).uri, :max_age => 12
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert camper.age <= 12
    end
    
    #assert mix of min and max age query
    get :index, :camp_id => camps(:bolo).uri, :min_age => 8, :max_age => 12
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert camper.age >= 8 and camper.age <= 12
    end
  end
  
  test "get campers by grade" do
    get :index, :camp_id => camps(:bolo).uri, :grade => 4
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert camper.grade == 4
    end
  end
  
  test "get campers by gender" do
    #assert query for male campers
    get :index, :camp_id => camps(:bolo).uri, :gender => "M"
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert camper.gender == "M"
    end
    
    #assert query for female campers
    get :index, :camp_id => camps(:bolo).uri, :gender => "F"
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert camper.gender == "F"
    end
  end
  
  test "get campers by program" do
    programId = programs(:traditional).id
    get :index, :camp_id => camps(:bolo).uri, :program_id => programId
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert_not_nil camper.programs.where("id = ?", programId)
    end
    
    programName = programs(:traditional).name
    get :index, :camp_id => camps(:bolo).uri, :program_name => programName
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert_not_nil camper.programs.where("name = ?", programName)
    end
  end
  
  test "get campers by session" do
    sessionId = sessions(:session_1).id
    sessionId2 = sessions(:session_2).id
    
    get :index, :camp_id => camps(:bolo).uri, :session_id => sessionId
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert_not_nil camper.sessions.where("id = ?", sessionId)
    end
    
    get :index, :camp_id => camps(:bolo).uri, :session_id => sessionId, :session_id => sessionId2
    assert_response :success
    
    campers = assigns(:campers)
    
    campers.each do |camper|
      assert_not_nil camper.sessions.where("id = ?", sessionId)
      assert_not_nil camper.sessions.where("id = ?", sessionId2)
    end
  end
  
  test "get campers by cabin" do
    flunk("cabins not implemented yet!")
  end
  
  test "get campers collection with authorized user" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :index, :camp_id => camps(:bolo).uri
    
    assert_response :success
    assert_not_nil assigns(:campers)
  end
  
  test "get all campers using unauthrorized camp_admin" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    get :index, :camp_id => camps(:bolo).uri
    
    assert_response :unauthorized
  end
  
  test "get all campers using unauthrorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :index, :camp_id => camps(:bolo).uri
    
    assert_response :unauthorized
  end
  
  test "register a camper with a camp" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
    
    assert_response :created
    assert_equal campers(:two).programs.count, 1, "the camper should have 1 program!"
    assert_equal campers(:two).sessions.count, 1, "the camper should have 1 session!"
    assert_equal program_populations(:one).current_population, 1, "the population should be 1!"
    
    camperTwoId = campers(:two).id
    assert_equal camps(:bolo).campers.find(camperTwoId).first_name, campers(:two).first_name 
    assert_equal camps(:bolo).campers.size, 4
  end
  
  test "try to register a camper with a bad request" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id
      
    assert_response :bad_request
    assert_equal campers(:two).programs.size, 0
    assert_equal campers(:two).sessions.size, 0
    
  end
  
  test "try to register a camper to a full program session" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_2).id
        }
      ]
      
    assert_response :bad_request
    assert program_populations(:full).current_population = program_populations(:full).max_population
  end
  
  test "try to register a camper to a program not linked to given session" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:horse_camp).id,
          :session_id => sessions(:session_4).id
        }
      ]
      
    assert_response :bad_request
    assert_equal programs(:horse_camp).campers.size, 0
  end
  
  test "try to register a camper to a program that is not part of the camp" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:tc_program).id,
          :session_id => sessions(:session_1).id
        }
      ]
      
    assert_response :bad_request
    assert_equal programs(:horse_camp).campers.size, 0
  end
  
  test "try to register a duplicate camper" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:one).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
    
    assert_response :conflict
    assert_equal camps(:bolo).campers.size, 3
    assert_equal program_populations(:one).current_population, 0, "the population should be 1!"
  end
  
  test "register a camper that is too young for a program" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:young2).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
      
      assert_response :bad_request
      assert_equal programs(:traditional).campers.size, 2
      assert_equal camps(:bolo).campers.size, 3
      assert_equal program_populations(:one).current_population, 0, "the population should be 1!"
  end
  
  test "register a camper that is too old for a program" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:old2).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
      
      assert_response :bad_request
      assert_equal programs(:traditional).campers.size, 2
      assert_equal camps(:bolo).campers.size, 3
      assert_equal program_populations(:one).current_population, 0, "the population should be 1!"
  end
  
  test "register an unkown camper" do
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => "_", 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
    
    assert_response :not_found
    assert_equal camps(:bolo).campers.size, 3
  end
  
  test "register a camper to an unkown camp" do
    post :create, 
      :camp_id => "_",    
      :camper_id =>  campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
    
    assert_response :not_found
    assert_equal campers(:two).camps.size, 0
  end
  
  test "register a camper with an authorized user" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
    
    assert_response :created
    assert_equal campers(:two).programs.count, 1, "the camper should have 1 program!"
    assert_equal campers(:two).sessions.count, 1, "the camper should have 1 session!"
    assert_equal program_populations(:one).current_population, 1, "the population should be 1!"
    
    camperTwoId = campers(:two).id
    assert_equal camps(:bolo).campers.find(camperTwoId).first_name, campers(:two).first_name 
    assert_equal camps(:bolo).campers.size, 4
  end
  
  test "try to register a camper with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
    
    assert_response :unauthorized
    assert_equal campers(:two).programs.count, 0, "the camper should have 0 program!"
    assert_equal campers(:two).sessions.count, 0, "the camper should have 0 session!"
    assert_equal program_populations(:one).current_population, 0, "the population should be 0!"
    assert_equal camps(:bolo).campers.size, 3
  end
  
  test "register a camper with an unauthorized camp_admin" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    post :create, 
      :camp_id => camps(:bolo).uri,    
      :camper_id => campers(:two).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
    
    assert_response :unauthorized
    assert_equal campers(:two).programs.count, 0, "the camper should have 0 program!"
    assert_equal campers(:two).sessions.count, 0, "the camper should have 0 session!"
    assert_equal program_populations(:one).current_population, 0, "the population should be 0!"
    assert_equal camps(:bolo).campers.size, 3
  end
  
  #*****************
  # Test /camps/{camp_id}/campers/{id}
  #*****************
  test "register a camper with another program and session in the camp" do
    put :update, 
      :camp_id => camps(:bolo).uri,    
      :id => campers(:one).id, 
      :programs => [
        {
          :program_id => programs(:horse_camp).id,
          :session_id => sessions(:session_2).id
        }
      ]
    
    assert_response :accepted
    assert_equal campers(:one).programs.count, 3, "the camper should have 2 programs!"
    assert_equal campers(:one).sessions.count, 3, "the camper should have 2 sessions!"
    assert_equal program_populations(:hc_two).current_population, 1, "the population should be 1!"
  end
  
  test "update a camper with a bad request" do
    put :update, 
      :camp_id => camps(:bolo).uri,    
      :id => campers(:one).id
      
    assert_response :bad_request
    assert_equal campers(:two).programs.size, 0
    assert_equal campers(:two).sessions.size, 0
  end
  
  test "try to update an unknown camper" do
    put :update, 
      :camp_id => camps(:bolo).uri,    
      :id => "_"
      
    assert_response :not_found
    assert_equal camps(:bolo).campers.size, 3
  end
  
  test "try to register a camper to a program and session they already registered to" do
    put :update, 
      :camp_id => camps(:bolo).uri,    
      :id => campers(:one).id, 
      :programs => [
        {
          :program_id => programs(:traditional).id,
          :session_id => sessions(:session_1).id
        }
      ]
      
    assert_response :bad_request
    assert_equal campers(:one).programs.count, 2, "the camper should have 2 programs!"
    assert_equal campers(:one).sessions.count, 2, "the camper should have 2 sessions!"
    assert_equal program_populations(:one).current_population, 0, "the population should be 1!"
  end
  
  test "update a camper with an authorized user" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    put :update, 
      :camp_id => camps(:bolo).uri,    
      :id => campers(:one).id, 
      :programs => [
        {
          :program_id => programs(:horse_camp).id,
          :session_id => sessions(:session_2).id
        }
      ]
    
    assert_response :accepted
    assert_equal campers(:one).programs.count, 3, "the camper should have 2 programs!"
    assert_equal campers(:one).sessions.count, 3, "the camper should have 2 sessions!"
    assert_equal program_populations(:hc_two).current_population, 1, "the population should be 1!"
  end
  
  test "update a camper with an unauthorized camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    put :update, 
      :camp_id => camps(:bolo).uri,    
      :id => campers(:one).id, 
      :programs => [
        {
          :program_id => programs(:horse_camp).id,
          :session_id => sessions(:session_2).id
        }
      ]
    
    assert_response :unauthorized
    assert_equal campers(:one).programs.count, 2, "the camper should have 2 programs!"
    assert_equal campers(:one).sessions.count, 2, "the camper should have 2 sessions!"
    assert_equal program_populations(:two).current_population, 0, "the population should be 1!"
  end
  
  test "try to update a camper with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    put :update, 
      :camp_id => camps(:bolo).uri,    
      :id => campers(:one).id, 
      :programs => [
        {
          :program_id => programs(:horse_camp).id,
          :session_id => sessions(:session_2).id
        }
      ]
    
    assert_response :unauthorized
    assert_equal campers(:one).programs.count, 2, "the camper should have 2 programs!"
    assert_equal campers(:one).sessions.count, 2, "the camper should have 2 sessions!"
    assert_equal program_populations(:two).current_population, 0, "the population should be 1!"
  end
  
  test "get a camper that is registered with the camp" do
    get :show, 
      :id => campers(:one).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :success
  end
  
  test "get an unknown camper" do
    get :show, 
      :id => "_",
      :camp_id => camps(:bolo).uri
      
    assert_response :not_found
  end
  
  test "get a camper with an authorized user" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :show, 
      :id => campers(:one).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :success
  end
  
  test "get a camper with an unauthorized camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    get :show, 
      :id => campers(:one).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :unauthorized
  end
  
  test "get a camper with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :show, 
      :id => campers(:one).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :unauthorized
  end

end
