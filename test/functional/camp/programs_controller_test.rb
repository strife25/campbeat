require 'test_helper'

class Camp::ProgramsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  def setup
    sign_in users(:dev)
  end
  
  #*****************
  # Test routes
  #*****************
  test "programs resource routes" do
    assert_routing '/camps/bolo/programs', { :controller => "camp/programs", :action => "index", :camp_id => "bolo"  }
    assert_routing '/camps/bolo/programs/1', { :controller => "camp/programs", :action => "show", :id => "1", :camp_id => "bolo" }
  end
  
  #*****************
  # Test /camps/{camp_id}/programs
  #*****************
  test "should get programs collection" do
    get :index, :camp_id => camps(:bolo).uri
    
    assert_response :success
    assert_not_nil assigns(:programs)
  end
  
  test "get programs collection with age query" do
    #assert min age query
    get :index, :camp_id => camps(:bolo).uri, :min_age => 11
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      assert program.min_age >= 11
    end
    
    #assert max age query
    get :index, :camp_id => camps(:bolo).uri, :max_age => 15
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      assert program.max_age <= 15
    end
    
    #assert mix of min and max age
    get :index, :camp_id => camps(:bolo).uri, :min_age => 11, :max_age => 15
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      assert program.min_age >= 11
      assert program.max_age <= 15
    end
  end
  
  test "get programs collection with price query" do
    #assert min price query
    get :index, :camp_id => camps(:bolo).uri, :min_price => 380
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      assert program.price >= 380.00
    end
    
    #assert max price query
    get :index, :camp_id => camps(:bolo).uri, :max_price => 450
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      assert program.price <= 450.00
    end
    
    #assert mix of min and max price query
    get :index, :camp_id => camps(:bolo).uri, :min_price => 380, :max_price => 450
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      assert program.price >= 380.00
      assert program.price <= 450.00
    end
  end
  
  test "get programs collection with date query" do
    #assert start_date query
    get :index, :camp_id => camps(:bolo).uri, :start_date => 2.weeks.ago 
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      program.sessions.each do |session|
        assert session.start_date.to_date  >= 2.weeks.ago.to_date
      end
    end
    
    #assert end_date query
    get :index, :camp_id => camps(:bolo).uri, :end_date => 3.weeks.from_now
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      program.sessions.each do |session|
        assert session.end_date.to_date <= 3.weeks.from_now.to_date
      end
    end
    
    #assert star and end date queries
    get :index, :camp_id => camps(:bolo).uri, :start_date => 2.weeks.ago , :end_date => 3.weeks.from_now
    assert_response :success
    
    programs = assigns(:programs)
    
    programs.each do |program|
      program.sessions.each do |session|
        assert session.start_date.to_date >= 2.weeks.ago.to_date
        assert session.end_date.to_date <= 3.weeks.from_now.to_date
      end
    end
  end
  
  test "get collection of open programs" do
    get :index, :camp_id => camps(:bolo).uri, :availablity=> "vacant"
    assert_response :success

    programs = assigns(:programs)
    
    programs = assigns(:programs)
    programs.each do |program|
      assert_not_nil program.program_populations.where('current_population < max_population')
    end
  end
  
  test "get collection of closed programs" do
    get :index, :camp_id => camps(:bolo).uri, :availablity=> "closed"
    assert_response :success
    
    programs = assigns(:programs)
    programs.each do |program|
      assert_not_nil program.program_populations.where('current_population >= max_population')
    end
  end
  
  test "only authorized users can access programs collection" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :index, :camp_id => camps(:bolo).uri
    assert_response :success
  end
  
  test "unauthorized request to programs collection with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    get :index, :camp_id => camps(:bolo).uri
    assert_response :unauthorized
  end
  
  test "unauthorized request to programs collection" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :index, :camp_id => camps(:bolo).uri
    assert_response :unauthorized
  end

  test "should create program" do
    assert_difference('Program.count') do
      post :create, 
        :program => {
          :name => "Test Program",
          :min_age => 10,
          :price => 100.00
        }, 
        :sessions => [
          {
            :session_id => sessions(:session_1).id,
            :max_population => 100
          },
          {
            :session_id => sessions(:session_2).id,
            :max_population => 200
          }
        ],
        :camp_id => camps(:bolo).uri
    end

    assert_response :created
    assert_not_nil assigns(:program)

    get :show, 
      :id => assigns(:program).to_param, 
      :camp_id => camps(:bolo).uri
       
    assert_response :success
    assert_equal assigns(:program).program_populations.first.max_population, 100
     assert_equal \
       assigns(:program).sessions.find(sessions(:session_1).id).start_date.to_s(:db), \
        1.week.ago.strftime("%Y-%m-%d"), \
        "the traditional program's session start date is not equal to 1 week ago"
    
    assert_equal assigns(:program).program_populations.last.max_population, 200
  end
  
  test "create a program with a bad request" do
    #create a program without a name
    post :create, 
      :program => {
        :min_age => 10,
        :price => 100.00,   
      },
      :camp_id => camps(:bolo).uri

    assert_response :bad_request
  end
  
  test "authorized request to create a program" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    assert_difference('Program.count') do
      post :create, 
        :program => {
          :name => "Test Program",
          :min_age => 10,
          :price => 100.00
        }, 
        :sessions => [
          {
            :session_id => sessions(:session_1).id,
            :max_population => 100
          },
          {
            :session_id => sessions(:session_2).id,
            :max_population => 200
          }
        ],
        :camp_id => camps(:bolo).uri
    end

    assert_response :created
  end
  
  test "unauthorized access to create a camp program with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    post :create, 
      :program => {
        :name => "Authorized Program",
        :min_age => 10,
        :price => 100.00  
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  test "unauthorized access to create a camp program" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    post :create, 
      :program => {
        :name => "Authorized Program",
        :min_age => 10,
        :price => 100.00  
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  #*****************
  # Test /camps/{camp_id}/programs/{id}
  #*****************
  test "should show program" do
    get :show, 
      :id => programs(:traditional).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :success
  end
  
  test "authorized get request on a camp program resource" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :show, 
      :id => programs(:traditional).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :success
  end
  
  test "unauthorized get request on a camp program resource with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    get :show, 
      :id => programs(:traditional).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :unauthorized
  end
  
  test "unauthorized get request on a camp program resource" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :show, 
      :id => programs(:traditional).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :unauthorized
  end
  
  test "get request on unknown camp program" do
    get :show, :id => "_", :camp_id => camps(:bolo).uri
    assert_response :missing
  end
  
  test "should update program" do
    post :create, 
      :program => {
        :name => "Test Update Program",
        :min_age => 10,
        :price => 100.00  
      },
      :sessions => [
        {
          :session_id => sessions(:session_1).id,
          :max_population => 100
        },
        {
          :session_id => sessions(:session_2).id,
          :max_population => 200
        }
      ],
      :camp_id => camps(:bolo).uri

    put :update, 
      :id => assigns(:program).to_param, 
      :program => {
        :name => "New Program Name"
      },
      :sessions => [
        {
          :session_id => sessions(:session_1).id,
          :max_population => 100
        }
      ],
      :camp_id => camps(:bolo).uri

    assert_response :accepted
    assert_equal assigns(:program).name, "New Program Name"
    assert_equal assigns(:program).program_populations.size, 1
    assert_equal assigns(:program).sessions.size, 1
  end
  
  test "authorized access to update a program" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    post :create, 
      :program => {
        :name => "Test Update Program",
        :min_age => 10,
        :price => 100.00  
      },
      :sessions => [
        {
          :session_id => sessions(:session_1).id,
          :max_population => 100
        },
        {
          :session_id => sessions(:session_2).id,
          :max_population => 200
        }
      ],
      :camp_id => camps(:bolo).uri

    put :update, 
      :id => assigns(:program).to_param, 
      :program => {
        :name => "New Program Name"
      },
      :sessions => [
        {
          :session_id => sessions(:session_1).id,
          :max_population => 100
        }
      ],
      :camp_id => camps(:bolo).uri

    assert_response :accepted
    assert_equal assigns(:program).name, "New Program Name"
  end
  
  test "update a progarm with unauthorized access with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    put :update, 
      :id => programs(:traditional).to_param, 
      :program => {
        :name => "New Program Name"
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  test "update a progarm with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    put :update, 
      :id => programs(:traditional).to_param, 
      :program => {
        :name => "New Program Name"
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  test "update a program with a bad request" do
    put :update, 
      :id => programs(:traditional).to_param, 
      :program => {
        :min_age => "a" 
      },
      :camp_id => camps(:bolo).uri
       
    assert_response :bad_request
  end
  
  test "update unknown program" do
    put :update, :id => "_", 
      :program => {
        :min_age => 10 
      },
      :camp_id => camps(:bolo).uri
      
    assert_response :missing
  end
  
  test "should destroy program" do
    assert_difference('Program.count', -1) do
      delete :destroy, 
        :id => programs(:traditional).to_param,
        :camp_id => camps(:bolo).uri
    end

    assert_response :accepted
  end

  test "authorized user can destroy a program" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    assert_difference('Program.count', -1) do
      delete :destroy, 
        :id => programs(:traditional).to_param,
        :camp_id => camps(:bolo).uri
    end

    assert_response :accepted
  end
  
  test "try to destroy session with unauthorized access with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    delete :destroy, 
      :id => programs(:traditional).to_param,
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end

  test "try to destroy session with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    delete :destroy, 
      :id => programs(:traditional).to_param,
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end

  test "delete unknown program" do
    delete :destroy, 
      :id => "_",
      :camp_id => camps(:bolo).uri
    assert_response :missing
  end
end
