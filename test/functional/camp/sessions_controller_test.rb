require 'test_helper'

class Camp::SessionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  def setup
    sign_in users(:dev)
  end
  
  #*****************
  # Test routes
  #*****************
  test "sessions resource routes" do
    assert_routing '/camps/bolo/sessions', { :controller => "camp/sessions", :action => "index", :camp_id => "bolo"  }
    assert_routing '/camps/bolo/sessions/1', { :controller => "camp/sessions", :action => "show", :id => "1", :camp_id => "bolo" }
  end
 
  #*****************
  # Test /camps/{camp_id}/sessions
  #*****************
  test "should get sessions collection" do
    get :index, :camp_id => camps(:bolo).uri
    
    assert_response :success
    assert_not_nil assigns(:sessions)
  end
  
  test "only authorized users can access sessions collection" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :index, :camp_id => camps(:bolo).uri
    assert_response :success
  end
  
  test "unauthorized request to sessions collection with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    get :index, :camp_id => camps(:bolo).uri
    assert_response :unauthorized
  end
  
  test "unauthorized request to sessions collection" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :index, :camp_id => camps(:bolo).uri
    assert_response :unauthorized
  end
  
  test "get sessions by date query" do
    #assert start_date query
    get :index, :camp_id => camps(:bolo).uri, :start_date => 1.day.ago 
    assert_response :success
    
    sessions = assigns(:sessions)
    
    sessions.each do |session|
      assert session.start_date.to_date  >= 1.day.ago.to_date
    end
    
    #assert end_date query
    get :index, :camp_id => camps(:bolo).uri, :end_date => 3.weeks.from_now
    assert_response :success
    
    sessions = assigns(:sessions)
    
    sessions.each do |session|
      assert session.start_date.to_date  <= 3.weeks.from_now.to_date
    end
    
    #assert start and end date queries
    get :index, :camp_id => camps(:bolo).uri, :start_date => 1.day.ago, :end_date => 3.weeks.from_now
    assert_response :success
    
    sessions = assigns(:sessions)
    
    sessions.each do |session|
      assert session.start_date.to_date  >= 1.day.ago.to_date
      assert session.start_date.to_date  <= 3.weeks.from_now.to_date
    end
  end
  
  test "get sessions by program name" do
    get :index, :camp_id => camps(:bolo).uri, :program_name => "Traditional Camp"
    assert_response :success
    
    sessions = assigns(:sessions)
    
    sessions.each do |session|
      assert_not_nil session.programs.where('name = ?', "Traditional Camp")
    end
  end
  
  test "get open sessions" do
    get :index, :camp_id => camps(:bolo).uri, :availability => 'vacant'
    assert_response :success
    
    sessions = assigns(:sessions)
    
    sessions.each do |session|
      assert_not_nil session.program_populations.where('current_population < max_population')
    end
  end
  
  test "get closed sessions" do
    get :index, :camp_id => camps(:bolo).uri, :availability => 'closed'
    assert_response :success
    
    sessions = assigns(:sessions)
    
    sessions.each do |session|
      assert_not_nil session.program_populations.where('current_population >= max_population')
    end
  end

  test "should create session" do
    assert_difference('Session.count') do
      post :create, 
        :session => {
          :start_date => 1.week.from_now.to_s(:db),
          :end_date => 2.week.from_now.to_s(:db) 
        },
        :camp_id => camps(:bolo).uri
    end

    assert_response :created
    assert_not_nil assigns(:session)

    get :show, 
      :id => assigns(:session).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :success
  end
  
  test "create a session with a bad request" do
    #create a program without a name
    post :create, 
      :session => {
        :start_date => Time.now.to_s(:db)
      },
      :camp_id => camps(:bolo).uri

    assert_response :bad_request
  end
  
  test "authorized users can create a session" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    post :create, 
      :session => {
        :start_date => 1.week.from_now.to_s(:db),
        :end_date => 2.week.from_now.to_s(:db) 
      },
      :camp_id => camps(:bolo).uri

    assert_response :created
  end
  
  test "unauthorized access to create a camp session with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    post :create, 
      :session=> {
        :start_date => Time.now.to_s(:db),
        :end_date => 1.week.from_now.to_s(:db) 
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  test "unauthorized access to create a camp session" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    post :create, 
      :session=> {
        :start_date => Time.now.to_s(:db),
        :end_date => 1.week.from_now.to_s(:db) 
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  #*****************
  # Test /camps/{camp_id}/programs/{id}
  #*****************
  test "should show session" do
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :success
  end
  
  test "authorized request for a session resource" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :success
  end
  
  test "unauthorized get request on a camp session resource with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :unauthorized
  end
  
  test "unauthorized get request on a camp session resource" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    get :show, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri
      
    assert_response :unauthorized
  end
  
  test "get request on unknown camp session" do
    get :show, 
      :id => "_",
      :camp_id => camps(:bolo).uri
      
    assert_response :missing
  end
  
  test "should update session" do
    post :create, 
      :session=> {
        :start_date => 1.week.from_now.to_s(:db),
        :end_date => 2.week.from_now.to_s(:db) 
      },
      :camp_id => camps(:bolo).uri

    put :update, 
      :id => assigns(:session).to_param, 
      :session => {
        :end_date => 3.week.from_now.to_s(:db) 
      },
      :camp_id => camps(:bolo).uri

    assert_response :accepted
    assert_equal assigns(:session).end_date.to_date, 3.week.from_now.to_date
  end
  
  test "authorized request to update a session" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    post :create, 
      :session=> {
        :start_date => 1.week.from_now.to_s(:db),
        :end_date => 2.week.from_now.to_s(:db) 
      },
      :camp_id => camps(:bolo).uri

    put :update, 
      :id => assigns(:session).to_param, 
      :session => {
        :end_date => 3.week.from_now.to_s(:db) 
      },
      :camp_id => camps(:bolo).uri

    assert_response :accepted
  end
  
  test "update a session with unauthorized access with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    put :update, 
      :id => sessions(:session_1).to_param, 
      :session => {
        :end_date => 3.week.from_now.to_s(:db)
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  test "update a session with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    put :update, 
      :id => sessions(:session_1).to_param, 
      :session => {
        :end_date => 3.week.from_now.to_s(:db)
      },
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  test "update a session with a bad request" do
    put :update, 
      :id => sessions(:session_1).to_param, 
      :session => {
        :end_date => "a" 
      },
      :camp_id => camps(:bolo).uri
      
    assert_response :bad_request
  end
  
  test "update unknown session" do
    put :update, 
      :id => "_", 
      :session => {
        :end_date => 3.week.from_now.to_s(:db)
      },
      :camp_id => camps(:bolo).uri

    assert_response :missing
  end
  
  test "should destroy session" do
    assert_difference('Session.count', -1) do
      delete :destroy, 
        :id => sessions(:session_1).to_param,
        :camp_id => camps(:bolo).uri
    end

    assert_response :accepted
  end

  test "try to destroy session with authorized access" do
    sign_out users(:dev)
    sign_in users(:boloAdmin)
    
    delete :destroy, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri

    assert_response :accepted
  end
  
  test "try to destroy session with unauthorized access with camp_admin user" do
    sign_out users(:dev)
    sign_in users(:tcAdmin)
    
    delete :destroy, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end
  
  test "try to destroy session with unauthorized access" do
    sign_out users(:dev)
    sign_in users(:parent)
    
    delete :destroy, 
      :id => sessions(:session_1).to_param,
      :camp_id => camps(:bolo).uri

    assert_response :unauthorized
  end

  test "delete unknown session" do
    delete :destroy, 
      :id => "_",
      :camp_id => camps(:bolo).uri
      
    assert_response :missing
  end
  
  
end
  