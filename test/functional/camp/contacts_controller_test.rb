require 'test_helper'

class Camp::ContactsControllerTest < ActionController::TestCase
    include Devise::TestHelpers

    def setup
      sign_in users(:dev)
    end
    
    #*****************
    # Test routes
    #*****************
    test "contacts resource routes" do
      assert_routing '/camps/bolo/contacts', { :controller => "camp/contacts", :action => "index", :camp_id => "bolo"  }
      assert_routing '/camps/bolo/contacts/1', { :controller => "camp/contacts", :action => "show", :id => "1", :camp_id => "bolo" }
    end
    
    #*****************
    # Test /camps/{camp_id}/contacts
    #*****************
    test "should get contacts collection" do
      get :index, :camp_id => camps(:bolo).uri
      assert_response :success
      assert_not_nil assigns(:contacts)
    end

    test "only authorized users can access contacts collection" do
      sign_out users(:dev)
      sign_in users(:boloAdmin)

      get :index, :camp_id => camps(:bolo).uri
      assert_response :success
    end
    
    test "unauthorized camp_admin cannot access contacts collection" do
      sign_out users(:dev)
      sign_in users(:tcAdmin)

      get :index, :camp_id => camps(:bolo).uri
      assert_response :unauthorized
    end
    
    test "unauthorized users cannot access contacts collection" do
      sign_out users(:dev)
      sign_in users(:parent)

      get :index, :camp_id => camps(:bolo).uri
      assert_response :unauthorized
    end

    test "should create contact" do
      assert_difference('Contact.count') do
        post :create, 
          :contact => {
            :first_name => "John",
            :last_name => "Doe",
            :email_address => "test@test.com",
            :day_phone => "1234567890",
            :evening_phone => "1234567890",
            :mobile_phone => "1234567890"
          },
          :camp_id => camps(:bolo).uri
      end

      assert_response :created
      assert_not_nil assigns(:contact)

      get :show, 
        :id => assigns(:contact).to_param,
        :camp_id => camps(:bolo).uri
        
      assert_response :success
    end

    test "create a contact with a bad request" do
      post :create, 
        :contact => { 
          :last_name => "Doe",
          :email_address => "test@test.com",
          :day_phone => "1234567890",
          :evening_phone => "1234567890",
          :mobile_phone => "1234567890"
        },
        :camp_id => camps(:bolo).uri

      assert_response :bad_request
    end

    test "only authorized users can create a camp" do
      sign_out users(:dev)
      sign_in users(:boloAdmin)
      
      post :create, 
        :contact => {
          :first_name => "John",
          :last_name => "Doe",
          :email_address => "test@test.com",
          :day_phone => "1234567890",
          :evening_phone => "1234567890",
          :mobile_phone => "1234567890"
        }, 
        :camp_id => camps(:bolo).uri

      assert_response :success
    end
    
    test "unauthorized access to create a camp contact" do
      sign_out users(:dev)
      sign_in users(:parent)
      
      post :create, 
        :contact => {
          :first_name => "John",
          :last_name => "Doe",
          :email_address => "test@test.com",
          :day_phone => "1234567890",
          :evening_phone => "1234567890",
          :mobile_phone => "1234567890"
        }, 
        :camp_id => camps(:bolo).uri

      assert_response :unauthorized
    end

    #*****************
    # Test /camps/{camp_id}/contacts/{id}
    #*****************
    test "should show contact" do
      get :show, 
        :id => contacts(:contact_john).to_param, 
        :camp_id => camps(:bolo).uri
        
      assert_response :success
    end

    test "authorized get request on a camp contact resource" do
      sign_out users(:dev)
      sign_in users(:boloAdmin)
      
      get :show, 
        :id => contacts(:contact_john).to_param,
        :camp_id => camps(:bolo).uri
        
      assert_response :success
    end
    
    test "unauthorized get request from a different camp_admin user" do 
      sign_out users(:dev)
      sign_in users(:tcAdmin)
      
      get :show, 
        :id => contacts(:contact_john).to_param,
        :camp_id => camps(:bolo).uri
        
      assert_response :unauthorized
    end
    
    test "unauthorized get request on a camp contact resource" do
      sign_out users(:dev)
      sign_in users(:parent)
      
      get :show, 
        :id => contacts(:contact_john).to_param,
        :camp_id => camps(:bolo).uri
        
      assert_response :unauthorized
    end

    test "get request on unknown camp contact" do
      get :show, 
        :id => "_",
        :camp_id => camps(:bolo).uri
        
      assert_response :missing
    end

    test "should update contact" do
      post :create, 
        :contact => { 
          :first_name => "Bill",
          :last_name => "Doe",
          :email_address => "bill@bolo.com",
          :day_phone => "1234567890",
          :evening_phone => "1234567890",
          :mobile_phone => "1234567890"
        }, 
        :camp_id => camps(:bolo).uri

      put :update, 
        :id => assigns(:contact).to_param, 
        :camp_id => camps(:bolo).uri,
        :contact => {
          :email_address => "new@bolo.com"
        }

      assert_response :accepted
      assert_equal assigns(:contact).email_address, "new@bolo.com"
    end

    test "update a camp contact with unauthorized access" do
      sign_out users(:dev)
      sign_in users(:parent)
      
      put :update, 
        :id => contacts(:contact_john).to_param, 
        :camp_id => camps(:bolo).uri,
        :contact => {
          :email_address => "new@bolo.com"
        }

      assert_response :unauthorized
      assert_equal contacts(:contact_john).email_address, "john@bolo.com"
    end
    
    test "update a camp contact with authorized access" do
      sign_out users(:dev)
      sign_in users(:boloAdmin)
      
      post :create, 
        :contact => { 
          :first_name => "Bill",
          :last_name => "Doe",
          :email_address => "bill@bolo.com",
          :day_phone => "1234567890",
          :evening_phone => "1234567890",
          :mobile_phone => "1234567890"
        }, 
        :camp_id => camps(:bolo).uri

      put :update, 
        :id => assigns(:contact).to_param, 
        :camp_id => camps(:bolo).uri,
        :contact => {
          :email_address => "new@bolo.com"
        }

      assert_response :accepted
      assert_equal assigns(:contact).email_address, "new@bolo.com"
    end
    
    test "update a contact with unauthorized camp_admin" do
      sign_out users(:dev)
      sign_in users(:tcAdmin)
      
      put :update, 
        :id => contacts(:contact_john).to_param, 
        :camp_id => camps(:bolo).uri,
        :contact => {
          :email_address => "new@bolo.com"
        }

      assert_response :unauthorized
      assert_equal contacts(:contact_john).email_address, "john@bolo.com"
    end

    test "update a camp contact with a bad request" do
      put :update, 
        :id => contacts(:contact_john).to_param,
        :camp_id => camps(:bolo).uri,
        :contact => {
          :email_address => "t"
        }
        
      assert_response :bad_request
    end

    test "update unknown contact" do
      put :update, 
        :id => "_", 
        :camp_id => camps(:bolo).uri,
        :contact => {
          :email_address => "new@bolo.com"
        }

      assert_response :missing
    end

    test "should destroy contact" do
      assert_difference('Contact.count', -1) do
        delete :destroy, 
          :camp_id => camps(:bolo).uri,
          :id => contacts(:contact_bob).id
      end

      assert_response :accepted
    end
    
    test "try to destroy contact with authorized access" do
      sign_out users(:dev)
      sign_in users(:boloAdmin)
      
      assert_difference('Contact.count', -1) do
        delete :destroy, 
          :camp_id => camps(:bolo).uri,
          :id => contacts(:contact_bob).id
      end

      assert_response :accepted
    end
    
    test "try to destroy contact with unauthorized camp_admin" do
      sign_out users(:dev)
      sign_in users(:tcAdmin)
      
      delete :destroy, 
        :camp_id => camps(:bolo).uri,
        :id => contacts(:contact_john).id

      assert_response :unauthorized
    end

    test "try to destroy contact with unauthorized access" do
      sign_out users(:dev)
      sign_in users(:parent)
      
      delete :destroy, 
        :camp_id => camps(:bolo).uri,
        :id => contacts(:contact_bob).id

      assert_response :unauthorized
    end

    test "delete unknown contact" do
      delete :destroy, 
        :camp_id => camps(:bolo).uri,
        :id => "_"
      assert_response :missing
    end
  end