class ChangePhoneTypesInGuardians < ActiveRecord::Migration
  def self.up
     change_column :guardians, :day_phone, :string
     change_column :guardians, :evening_phone, :string
     change_column :guardians, :mobile_phone, :string
  end

  def self.down
    change_column :guardians, :day_phone, :integer
     change_column :guardians, :evening_phone, :integer
     change_column :guardians, :mobile_phone, :integer
  end
end
