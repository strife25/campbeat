class AddSuffixToCampers < ActiveRecord::Migration
  def self.up
    add_column :campers, :suffix, :string
  end

  def self.down
    remove_column :campers, :suffix
  end
end
