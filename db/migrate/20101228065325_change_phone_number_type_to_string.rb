class ChangePhoneNumberTypeToString < ActiveRecord::Migration
  def self.up
    change_column :contacts, :phone_number, :string
  end

  def self.down
    change_column :contacts, :phone_number, :integer
  end
end
