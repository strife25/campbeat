class RemoveTownFromAddress < ActiveRecord::Migration
  def self.up
    remove_column :addresses, :town
  end

  def self.down
    add_column :addresses, :town, :string
  end
end
