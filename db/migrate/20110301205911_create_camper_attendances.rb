class CreateCamperAttendances < ActiveRecord::Migration
  def self.up
    create_table :camper_attendances , :id => false do |t|
      t.integer :camper_id
      t.integer :program_id
      t.integer :session_id

      t.timestamps
    end
  end

  def self.down
    drop_table :camper_attendances
  end
end
