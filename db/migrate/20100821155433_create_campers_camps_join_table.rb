class CreateCampersCampsJoinTable < ActiveRecord::Migration
  def self.up
    create_table :campers_camps, :id => false do |t|
      t.references :camper
      t.references :camp
    end
  end

  def self.down
    drop_table :campers_camps
  end
end
