class CreatePrograms < ActiveRecord::Migration
  def self.up
    create_table :programs do |t|
      t.references :camp
      t.string :name
      t.string :description
      t.integer :min_age
      t.integer :max_age
      t.integer :cost

      t.timestamps
    end
  end

  def self.down
    drop_table :programs
  end
end
