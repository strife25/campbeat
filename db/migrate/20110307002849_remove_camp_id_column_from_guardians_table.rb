class RemoveCampIdColumnFromGuardiansTable < ActiveRecord::Migration
  def self.up
    remove_column :guardians, :camp_id
  end

  def self.down
    add_column :guardians, :camp_id, :integer
  end
end
