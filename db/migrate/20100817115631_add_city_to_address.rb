class AddCityToAddress < ActiveRecord::Migration
  def self.up
    add_column :addresses, :city, :string
  end

  def self.down
    remove_column :addresses, :town
  end
end
