class CreateCamps < ActiveRecord::Migration
  def self.up
    create_table :camps do |t|
      t.string :uri
      t.string :name
      t.string :web_address
      t.string :email_address
      t.string :street_1
      t.string :street_2
      t.string :city
      t.string :state
      t.integer :zip_code
      t.string :country

      t.timestamps
    end
  end

  def self.down
    drop_table :camps
  end
end
