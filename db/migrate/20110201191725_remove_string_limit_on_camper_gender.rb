class RemoveStringLimitOnCamperGender < ActiveRecord::Migration
  def self.up
    change_column :campers, :gender, :string
  end

  def self.down
    change_column :campers, :gender, :string, :limit => 1 
  end
end
