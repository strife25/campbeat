class AddPhonesToParent < ActiveRecord::Migration
  def self.up
    add_column :parents, :day_phone, :integer
    add_column :parents, :evening_phone, :integer
    add_column :parents, :mobile_phone, :integer
  end

  def self.down
    remove_column :parents, :mobile_phone
    remove_column :parents, :evening_phone
    remove_column :parents, :day_phone
  end
end
