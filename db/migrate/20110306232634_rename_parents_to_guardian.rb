class RenameParentsToGuardian < ActiveRecord::Migration
  def self.up
    rename_table :parents, :guardians
  end

  def self.down
    rename_table :guardians, :parents
  end
end
