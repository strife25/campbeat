class ChangeCostColumnNameToPrice < ActiveRecord::Migration
  def self.up
    rename_column :programs, :cost, :price
  end

  def self.down
    rename_column :programs, :price, :cost
  end
end
