class DropCamperSessionsProgramsTable < ActiveRecord::Migration
  def self.up
    drop_table :campers_sessions_programs
  end
  
  def self.down
    create_table :campers_sessions_programs, :id => false do |t|
      t.references :camper
      t.references :session
      t.references :program
    end
  end
end
