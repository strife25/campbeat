class RemoveAgeFromCampers < ActiveRecord::Migration
  def self.up
    remove_column :campers, :age
  end

  def self.down
    add_column :campers, :age, :integer
  end
end
