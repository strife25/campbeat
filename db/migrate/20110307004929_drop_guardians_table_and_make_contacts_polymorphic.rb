class DropGuardiansTableAndMakeContactsPolymorphic < ActiveRecord::Migration
  def self.up
    drop_table :guardians
    add_column :contacts, :contactable_id, :integer
    add_column :contacts, :contactable_type, :string
  end

  def self.down
    create_table :guardians do |t|
      t.references :camper
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :suffix
      t.string :email_address
      t.string :day_phone
      t.string :evening_phone
      t.string :mobile_phone
      t.timestamps
    end
    remove_column :contacts, :contactable_id
    remove_column :contacts, :contactable_type
  end
end
