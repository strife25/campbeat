class CreateCampCamperJoinTable < ActiveRecord::Migration
  def self.up
    create_table :camps_campers, :id => false do |t|
      t.integer :camper_id
      t.integer :camp_id
    end
  end

  def self.down
    drop_table :camps_campers
  end
end
