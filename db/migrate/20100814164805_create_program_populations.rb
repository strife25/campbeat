class CreateProgramPopulations < ActiveRecord::Migration
  def self.up
    create_table :program_populations do |t|
      t.references :program
      t.references :session
      t.integer :current_population
      t.integer :max_population

      t.timestamps
    end
  end

  def self.down
    drop_table :program_populations
  end
end
