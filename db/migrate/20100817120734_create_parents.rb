class CreateParents < ActiveRecord::Migration
  def self.up
    create_table :parents do |t|
      t.references :camp
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :suffix
      t.string :email_address

      t.timestamps
    end
  end

  def self.down
    drop_table :parents
  end
end
