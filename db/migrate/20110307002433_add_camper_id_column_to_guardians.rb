class AddCamperIdColumnToGuardians < ActiveRecord::Migration
  def self.up
    add_column :guardians, :camper_id, :integer
    drop_table :campers_guardians
  end

  def self.down
    remove_column :guardians, :camper_id
    create_table :campers_guardians, :id => false do |t|
      t.references :camper
      t.references :guardian
    end
  end
end
