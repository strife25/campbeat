class CreateCampers < ActiveRecord::Migration
  def self.up
    create_table :campers do |t|
      t.string :first_name
      t.string :middle_name
      t.string :list_name
      t.integer :age
      t.datetime :birthday
      t.integer :grade
      t.boolean :gender

      t.timestamps
    end
  end

  def self.down
    drop_table :campers
  end
end
