class CamperGuardian < ActiveRecord::Migration
  def self.up
    create_table :campers_guardians, :id => false do |t|
      t.references :camper
      t.references :guardian
    end
  end

  def self.down
    drop_table :campers_guardians
  end
end
