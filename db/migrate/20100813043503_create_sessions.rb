class CreateSessions < ActiveRecord::Migration
  def self.up
    create_table :sessions do |t|
      t.references :camp
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end

  def self.down
    drop_table :sessions
  end
end
