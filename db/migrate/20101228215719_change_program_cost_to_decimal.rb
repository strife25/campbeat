class ChangeProgramCostToDecimal < ActiveRecord::Migration
  def self.up
    change_column :programs, :cost, :decimal, :precision => 8, :scale => 2
  end

  def self.down
    change_column :programs, :cost, :integer
  end
end
