class AddDefaultValueToCurrentPopulation < ActiveRecord::Migration
  def self.up
    remove_column :program_populations, :current_population
    add_column :program_populations, :current_population, :integer, :default => '0'
  end

  def self.down
    remove_column :program_populations, :current_population
    add_column :program_populations, :current_population, :integer
  end
end
