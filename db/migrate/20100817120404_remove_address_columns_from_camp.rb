class RemoveAddressColumnsFromCamp < ActiveRecord::Migration
  def self.up
    remove_column :camps, :street_1
    remove_column :camps, :street_2
    remove_column :camps, :city
    remove_column :camps, :zip_code
    remove_column :camps, :country
  end

  def self.down
    add_column :camps, :country, :string
    add_column :camps, :zip_code, :integer
    add_column :camps, :city, :string
    add_column :camps, :street_2, :string
    add_column :camps, :street_1, :string
  end
end
