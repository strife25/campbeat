class FixCamperLastNameColumn < ActiveRecord::Migration
  def self.up
    add_column :campers, :last_name, :string
    remove_column :campers, :list_name
  end

  def self.down
    remove_column :campers, :last_name, :string
    add_column :campers, :list_name
  end
end
