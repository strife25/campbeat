class UsersHaveManyCampers < ActiveRecord::Migration
  def self.up
    add_column :campers, :user_id, :integer
  end

  def self.down
    remove_column :campers, :user_id
  end
end
