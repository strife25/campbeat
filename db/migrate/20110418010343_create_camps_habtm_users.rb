class CreateCampsHabtmUsers < ActiveRecord::Migration
  def self.up
    create_table :camps_users, :id => false do |t|
      t.references :camp, :user
    end
  end

  def self.down
    drop_table :camps_users
  end
end
