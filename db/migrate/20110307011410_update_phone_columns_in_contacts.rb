class UpdatePhoneColumnsInContacts < ActiveRecord::Migration
  def self.up
    remove_column :contacts, :phone_number
    add_column :contacts, :day_phone, :string
    add_column :contacts, :evening_phone, :string
    add_column :contacts, :mobile_phone, :string
  end

  def self.down
    add_column :contacts, :phone_number, :string
    remove_column :contacts, :day_phone, :string
    remove_column :contacts, :evening_phone, :string
    remove_column :contacts, :mobile_phone, :string
  end
end
