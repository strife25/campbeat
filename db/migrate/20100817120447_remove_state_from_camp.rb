class RemoveStateFromCamp < ActiveRecord::Migration
  def self.up
    remove_column :camps, :state
  end

  def self.down
    add_column :camps, :state, :string
  end
end
