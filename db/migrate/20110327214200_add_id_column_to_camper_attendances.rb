class AddIdColumnToCamperAttendances < ActiveRecord::Migration
  def self.up
    add_column :camper_attendances, :id, :primary_key
  end

  def self.down
    remove_column :camper_attendances, :id
  end
end
