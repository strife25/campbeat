class CreateCampersSessionsProgramsJoinTable < ActiveRecord::Migration
  def self.up
    create_table :campers_sessions_programs, :id => false do |t|
      t.references :camper
      t.references :session
      t.references :program
    end
  end

  def self.down
    drop_table :campers_sessions_programs
  end
end
