class CreateAddresses < ActiveRecord::Migration
  def self.up
    create_table :addresses do |t|
      t.references :addressable, :polymorphic => true
      t.string :street_1
      t.string :street_2
      t.string :town
      t.string :state
      t.integer :zip_code
      t.string :country

      t.timestamps
    end
  end

  def self.down
    drop_table :addresses
  end
end
