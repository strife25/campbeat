# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110516185036) do

  create_table "addresses", :force => true do |t|
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.string   "street_1"
    t.string   "street_2"
    t.string   "state"
    t.string   "zip_code"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
  end

  create_table "camper_attendances", :force => true do |t|
    t.integer  "camper_id"
    t.integer  "program_id"
    t.integer  "session_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "campers", :force => true do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.datetime "birthday"
    t.integer  "grade"
    t.string   "gender"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "last_name"
    t.string   "suffix"
    t.integer  "user_id"
  end

  create_table "campers_camps", :id => false, :force => true do |t|
    t.integer "camper_id"
    t.integer "camp_id"
  end

  create_table "camps", :force => true do |t|
    t.string   "uri"
    t.string   "name"
    t.string   "web_address"
    t.string   "email_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "camps_campers", :id => false, :force => true do |t|
    t.integer "camper_id"
    t.integer "camp_id"
  end

  create_table "camps_users", :id => false, :force => true do |t|
    t.integer "camp_id"
    t.integer "user_id"
  end

  create_table "contacts", :force => true do |t|
    t.integer  "camp_id"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "suffix"
    t.string   "email_address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "contactable_id"
    t.string   "contactable_type"
    t.string   "day_phone"
    t.string   "evening_phone"
    t.string   "mobile_phone"
  end

  create_table "program_populations", :force => true do |t|
    t.integer  "program_id"
    t.integer  "session_id"
    t.integer  "max_population"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "current_population", :default => 0
  end

  create_table "programs", :force => true do |t|
    t.integer  "camp_id"
    t.string   "name"
    t.string   "description"
    t.integer  "min_age"
    t.integer  "max_age"
    t.decimal  "price",       :precision => 8, :scale => 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "sessions", :force => true do |t|
    t.integer  "camp_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
