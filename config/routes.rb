Campbeat::Application.routes.draw do
  devise_for :users, :path_names => { 
    :sign_in => 'login', 
    :sign_out => 'logout' 
  }
  
  devise_scope :users do
    get "login", :to => "devise/sessions#new"
    get "logout", :to => "devise/sessions#destroy"
  end

  resources :campers
  
  resources :camps do
    match 'register' => "registrations#show", :via => :get
    match 'create_registration' => "registrations#create", :via => :post
    
    scope :module => "camp" do
      resources :campers do
        scope :module => "campers" do
          resources :sessions
        end
      end

      resources :contacts
      resources :sessions
      resources :programs
      resources :parents
    end
  end

  match '/:controller(/:action(/:id))'
  
  root :to => "pages#index"
end
