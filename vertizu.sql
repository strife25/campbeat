-- phpMyAdmin SQL Dump
-- version 3.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 31, 2010 at 02:11 PM
-- Server version: 5.1.42
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vertizu`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountTypes`
--

CREATE TABLE IF NOT EXISTS `accountTypes` (
  `accountTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `accountType` varchar(60) NOT NULL,
  `uidFieldName` varchar(40) NOT NULL,
  `mainTable` varchar(40) NOT NULL,
  PRIMARY KEY (`accountTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `accountTypes`
--

INSERT INTO `accountTypes` (`accountTypeID`, `accountType`, `uidFieldName`, `mainTable`) VALUES
(1, 'camper', 'camperID', 'campers'),
(2, 'parent', 'parentID', 'parents'),
(3, 'camp', 'campID', 'camps'),
(4, 'administrator', 'adminID', 'administrators');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `activityID` int(11) NOT NULL AUTO_INCREMENT,
  `campID` int(11) NOT NULL,
  `programSessionID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`activityID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `activities`
--


-- --------------------------------------------------------

--
-- Table structure for table `activityTimeSlotDays`
--

CREATE TABLE IF NOT EXISTS `activityTimeSlotDays` (
  `timeSlotDayID` int(11) NOT NULL,
  `timeSlotID` int(11) NOT NULL,
  `dayID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activityTimeSlotDays`
--


-- --------------------------------------------------------

--
-- Table structure for table `activityTimeSlotRelationships`
--

CREATE TABLE IF NOT EXISTS `activityTimeSlotRelationships` (
  `id` int(11) NOT NULL,
  `campID` int(11) NOT NULL,
  `activityID` int(11) NOT NULL,
  `timeSlotID` int(11) NOT NULL,
  `locationID` int(11) NOT NULL,
  `counselorID` int(11) NOT NULL,
  `availableSlots` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activityTimeSlotRelationships`
--


-- --------------------------------------------------------

--
-- Table structure for table `addressAssociations`
--

CREATE TABLE IF NOT EXISTS `addressAssociations` (
  `addressDetailsID` int(11) NOT NULL,
  `camperID` int(11) NOT NULL DEFAULT '0',
  `parentID` int(11) NOT NULL DEFAULT '0',
  `primary` tinyint(1) NOT NULL,
  UNIQUE KEY `addressDetailsID` (`addressDetailsID`,`camperID`),
  UNIQUE KEY `addressDetailsID_2` (`addressDetailsID`,`parentID`),
  KEY `parentID` (`parentID`),
  KEY `camperID` (`camperID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addressAssociations`
--


-- --------------------------------------------------------

--
-- Table structure for table `addressDetails`
--

CREATE TABLE IF NOT EXISTS `addressDetails` (
  `addressDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `streetAddress1` varchar(120) NOT NULL,
  `streetAddress2` varchar(60) NOT NULL,
  `city` varchar(80) NOT NULL,
  `stateID` int(11) NOT NULL,
  `dayPhone` varchar(20) NOT NULL,
  `eveningPhone` varchar(20) NOT NULL,
  `cellPhone` varchar(20) NOT NULL,
  PRIMARY KEY (`addressDetailsID`),
  KEY `stateID` (`stateID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `addressDetails`
--


-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `adminID` int(11) NOT NULL,
  `administratorFirstName` varchar(80) NOT NULL,
  `administratorLastName` varchar(80) NOT NULL,
  `administratorEmail` varchar(100) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`adminID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators`
--


-- --------------------------------------------------------

--
-- Table structure for table `ageGroups`
--

CREATE TABLE IF NOT EXISTS `ageGroups` (
  `ageGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `ageGroup` varchar(20) NOT NULL,
  PRIMARY KEY (`ageGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ageGroups`
--


-- --------------------------------------------------------

--
-- Table structure for table `campAdministratorRelationships`
--

CREATE TABLE IF NOT EXISTS `campAdministratorRelationships` (
  `campID` int(11) NOT NULL,
  `administratorID` int(11) NOT NULL,
  PRIMARY KEY (`campID`,`administratorID`),
  KEY `administratorID` (`administratorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campAdministratorRelationships`
--


-- --------------------------------------------------------

--
-- Table structure for table `camperActivities`
--

CREATE TABLE IF NOT EXISTS `camperActivities` (
  `camperID` int(11) NOT NULL,
  `activityTimeSlotID` int(11) NOT NULL,
  PRIMARY KEY (`camperID`,`activityTimeSlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `camperActivities`
--


-- --------------------------------------------------------

--
-- Table structure for table `camperInformation`
--

CREATE TABLE IF NOT EXISTS `camperInformation` (
  `camperID` int(11) NOT NULL,
  `camperAge` int(11) NOT NULL,
  `camperBirthMonthID` int(11) NOT NULL,
  `camperBirthDay` int(11) NOT NULL,
  `camperBirthYear` int(11) NOT NULL,
  `camperGrade` int(11) NOT NULL,
  `camperGender` enum('Male','Female') NOT NULL,
  KEY `camperID` (`camperID`),
  KEY `camperBirthMonthID` (`camperBirthMonthID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `camperInformation`
--


-- --------------------------------------------------------

--
-- Table structure for table `campers`
--

CREATE TABLE IF NOT EXISTS `campers` (
  `camperID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(80) NOT NULL,
  `middleInitial` varchar(1) NOT NULL DEFAULT '',
  `lastName` int(80) NOT NULL,
  `suffix` enum('','Jr.','Sr.','II','III','IV','V') NOT NULL DEFAULT '',
  `email` varchar(60) NOT NULL DEFAULT '',
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`camperID`),
  KEY `camperEmail` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `campers`
--


-- --------------------------------------------------------

--
-- Table structure for table `campPrograms`
--

CREATE TABLE IF NOT EXISTS `campPrograms` (
  `campProgramID` int(11) NOT NULL AUTO_INCREMENT,
  `campID` int(11) NOT NULL,
  `campProgramTypeID` int(11) NOT NULL,
  `campProgramName` varchar(120) NOT NULL,
  `campProgramDescription` text NOT NULL,
  `ageGroupID` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  PRIMARY KEY (`campProgramID`),
  KEY `ageGroupID` (`ageGroupID`),
  KEY `campID` (`campID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `campPrograms`
--


-- --------------------------------------------------------

--
-- Table structure for table `camps`
--

CREATE TABLE IF NOT EXISTS `camps` (
  `campID` int(11) NOT NULL AUTO_INCREMENT,
  `campName` varchar(100) NOT NULL,
  `campAddress1` varchar(100) NOT NULL,
  `campAddress2` varchar(100) NOT NULL,
  `campCity` varchar(80) NOT NULL,
  `campStateID` int(11) NOT NULL,
  `campZipCode` varchar(11) NOT NULL,
  `campWebsite` varchar(100) NOT NULL,
  `campContactName` varchar(80) NOT NULL,
  `campContactPhone` varchar(15) NOT NULL,
  `campContactEmail` varchar(100) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`campID`),
  KEY `campStateID` (`campStateID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3008 ;

--
-- Dumping data for table `camps`
--

INSERT INTO `camps` (`campID`, `campName`, `campAddress1`, `campAddress2`, `campCity`, `campStateID`, `campZipCode`, `campWebsite`, `campContactName`, `campContactPhone`, `campContactEmail`, `creation_date`) VALUES
(3000, 'Camp Marshall', '92 McCormick Rd.', '', 'Spencer', 23, '01562', 'http://www.campmarshall.com', 'Bobby', '', '', '2010-04-29 00:00:00'),
(3006, 'testcamp', '666 camp rd', '', 'na', 6, '445', 'camptest.com', 'Judas', '555.5455.555', 'judas@camptest.com', '2010-05-04 23:54:41'),
(3007, 'testcamp2', 'addressline 1', 'addressline2', 'Herndon', 50, '20170', 'testwebsite.com', 'Tim Tut', '555888', 'tim.tutt@gmaill.com', '2010-05-04 23:57:05');

-- --------------------------------------------------------

--
-- Table structure for table `campSessions`
--

CREATE TABLE IF NOT EXISTS `campSessions` (
  `sessionID` int(11) NOT NULL AUTO_INCREMENT,
  `sessionStateDate` date NOT NULL,
  `sessionEndDate` date NOT NULL,
  PRIMARY KEY (`sessionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `campSessions`
--


-- --------------------------------------------------------

--
-- Table structure for table `campTimeSlots`
--

CREATE TABLE IF NOT EXISTS `campTimeSlots` (
  `timeSlotID` int(11) NOT NULL AUTO_INCREMENT,
  `campID` int(11) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`timeSlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `campTimeSlots`
--


-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('1e5b0631d9a51ce05c8226fc4f52bf39', '198.81.129.193', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv', 1277296671, ''),
('74ab8cb747157dbdddd9a33ec5a653fb', '198.81.129.193', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv', 1277312161, ''),
('fd2a00f6f55f009c3c418a6511714ad6', '0.0.0.0', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en', 1277344218, ''),
('d6a2a4a3dad5b98465ed7c4746f024fe', '0.0.0.0', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en', 1277268861, 'a:6:{s:8:"loggedIn";s:1:"1";s:8:"username";s:4:"jode";s:8:"password";s:32:"8891c8043a7395b75b3db3cdb5ff4660";s:10:"userTypeID";s:1:"2";s:8:"userType";s:6:"parent";s:11:"displayName";s:4:"jode";}'),
('7d090e2727c8e65b6cd17a2fb910c4c3', '0.0.0.0', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; ', 1277266320, 'a:6:{s:8:"loggedIn";s:1:"1";s:8:"username";s:7:"jryding";s:8:"password";s:32:"5093c732b078e7aaf97c7dc9aa66eae3";s:10:"userTypeID";s:1:"2";s:8:"userType";s:6:"parent";s:11:"displayName";s:7:"jryding";}');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE IF NOT EXISTS `days` (
  `dayID` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(15) NOT NULL,
  `abbreviation` varchar(15) NOT NULL,
  PRIMARY KEY (`dayID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`dayID`, `day`, `abbreviation`) VALUES
(1, 'Sunday', 'Sun.'),
(2, 'Monday', 'Mon.'),
(3, 'Tuesday', 'Tues.'),
(4, 'Wednesday', 'Weds.'),
(5, 'Thursday', 'Thurs.'),
(6, 'Friday', 'Fri.'),
(7, 'Saturday', 'Sat.');

-- --------------------------------------------------------

--
-- Table structure for table `demographicInformation`
--

CREATE TABLE IF NOT EXISTS `demographicInformation` (
  `camperID` int(11) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `ageGroupID` int(11) NOT NULL,
  `ethnicityID` int(11) DEFAULT NULL,
  `raceID` int(11) DEFAULT NULL,
  `returningCamper` tinyint(1) NOT NULL DEFAULT '0',
  `4-H` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`camperID`),
  KEY `ageGroupID` (`ageGroupID`),
  KEY `ethnicityID` (`ethnicityID`),
  KEY `raceID` (`raceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demographicInformation`
--


-- --------------------------------------------------------

--
-- Table structure for table `ethnicities`
--

CREATE TABLE IF NOT EXISTS `ethnicities` (
  `ethnicityID` int(11) NOT NULL AUTO_INCREMENT,
  `ethnicity` varchar(30) NOT NULL,
  PRIMARY KEY (`ethnicityID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ethnicities`
--


-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `inventoryID` int(11) NOT NULL,
  `campID` int(11) NOT NULL,
  `activityID` int(11) NOT NULL,
  `itemName` varchar(80) NOT NULL,
  `itemdescription` text NOT NULL,
  `itemQuantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--


-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `locationID` int(11) NOT NULL AUTO_INCREMENT,
  `campID` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `latitude` varchar(40) NOT NULL,
  `longitude` varchar(40) NOT NULL,
  `geocode` varchar(40) NOT NULL,
  PRIMARY KEY (`locationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `locations`
--


-- --------------------------------------------------------

--
-- Table structure for table `months`
--

CREATE TABLE IF NOT EXISTS `months` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(10) NOT NULL,
  `abbreviation` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `months`
--

INSERT INTO `months` (`id`, `month`, `abbreviation`) VALUES
(1, 'January', 'Jan.'),
(2, 'February', 'Feb.'),
(3, 'March', 'Mar.'),
(4, 'April', 'Apr.'),
(5, 'May', 'May'),
(6, 'June', 'Jun.'),
(7, 'July', 'Jul.'),
(8, 'August.', 'Aug.'),
(9, 'September', 'Sept.'),
(10, 'October', 'Oct.'),
(11, 'November', 'Nov.'),
(12, 'December', 'Dec.');

-- --------------------------------------------------------

--
-- Table structure for table `parentCamperRelationships`
--

CREATE TABLE IF NOT EXISTS `parentCamperRelationships` (
  `parentID` int(11) NOT NULL,
  `camperID` int(11) NOT NULL,
  `relationship` enum('Mother','Father','Guardian') NOT NULL,
  PRIMARY KEY (`parentID`,`camperID`),
  KEY `camperID` (`camperID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parentCamperRelationships`
--


-- --------------------------------------------------------

--
-- Table structure for table `parentInformation`
--

CREATE TABLE IF NOT EXISTS `parentInformation` (
  `parentID` int(11) NOT NULL,
  `prefix` enum('Mr.','Mrs.','Miss','Ms.','Prof.','Ph.D.','Dr.','Rev.') NOT NULL,
  `firstName` varchar(80) NOT NULL,
  `middleInitial` varchar(1) NOT NULL,
  `lastName` varchar(80) NOT NULL,
  `email` varchar(100) NOT NULL,
  `relationship` enum('Mother','Father','Guardian') NOT NULL,
  PRIMARY KEY (`parentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parentInformation`
--

INSERT INTO `parentInformation` (`parentID`, `prefix`, `firstName`, `middleInitial`, `lastName`, `email`, `relationship`) VALUES
(1700, 'Mr.', 'Timothy', 'F', 'Tutt Jr.', 'ttutt@vt.edu', 'Father');

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE IF NOT EXISTS `parents` (
  `parentID` int(11) NOT NULL AUTO_INCREMENT,
  `parentFirstName` varchar(80) NOT NULL,
  `parentLastName` varchar(80) NOT NULL,
  `parentEmail` varchar(100) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `displayName` varchar(80) NOT NULL,
  PRIMARY KEY (`parentID`),
  KEY `parentEmail` (`parentEmail`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1706 ;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`parentID`, `parentFirstName`, `parentLastName`, `parentEmail`, `creation_date`, `displayName`) VALUES
(3, 'Michael', 'Clayton', 'michaelc@yahoo.com', '2010-05-06 01:29:51', ''),
(4, 'Michael', 'Clayton', 'michaelc@yahoo.com', '2010-05-06 01:30:15', ''),
(1700, 'Tim', 'Tutt', 'ttutt@vt.edu', '2010-05-03 21:19:22', ''),
(1702, 'Tim', 'Tutt', 'tim.tutt@gmail.com', '2010-05-19 17:57:03', ''),
(1703, 'John', 'Doe', 'jdoe@yahoo.com', '2010-05-23 08:13:38', ''),
(1704, 'John', 'Doe', 'jode@yahoo.com', '2010-06-06 18:19:09', ''),
(1705, 'John', 'Rydig', 'linus@johnryding.com', '2010-06-22 20:27:18', '');

-- --------------------------------------------------------

--
-- Table structure for table `programSessionRelationships`
--

CREATE TABLE IF NOT EXISTS `programSessionRelationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campID` int(11) NOT NULL,
  `progamID` int(11) NOT NULL,
  `sessionID` int(11) NOT NULL,
  `availableSlots` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `programSessionRelationships`
--


-- --------------------------------------------------------

--
-- Table structure for table `races`
--

CREATE TABLE IF NOT EXISTS `races` (
  `raceID` int(11) NOT NULL AUTO_INCREMENT,
  `race` varchar(80) NOT NULL,
  PRIMARY KEY (`raceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `races`
--


-- --------------------------------------------------------

--
-- Table structure for table `registrations`
--

CREATE TABLE IF NOT EXISTS `registrations` (
  `registrationID` int(11) NOT NULL AUTO_INCREMENT,
  `campID` int(11) NOT NULL,
  `camperID` int(11) NOT NULL,
  `programSessionID` int(11) NOT NULL,
  `registrationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `waitList` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`registrationID`),
  KEY `campID` (`campID`),
  KEY `camperID` (`camperID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `registrations`
--


-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `abbrev` varchar(2) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `abbrev`) VALUES
(1, 'Alaska', 'AK'),
(2, 'Alabama', 'AL'),
(3, 'American Samoa', 'AS'),
(4, 'Arizona', 'AZ'),
(5, 'Arkansas', 'AR'),
(6, 'California', 'CA'),
(7, 'Colorado', 'CO'),
(8, 'Connecticut', 'CT'),
(9, 'Delaware', 'DE'),
(10, 'District of Columbia', 'DC'),
(11, 'Florida', 'FL'),
(12, 'Georgia', 'GA'),
(13, 'Hawaii', 'HI'),
(14, 'Idaho', 'ID'),
(15, 'Illinois', 'IL'),
(16, 'Indiana', 'IN'),
(17, 'Iowa', 'IA'),
(18, 'Kansas', 'KS'),
(19, 'Kentucky', 'KY'),
(20, 'Louisiana', 'LA'),
(21, 'Maine', 'ME'),
(22, 'Maryland', 'MD'),
(23, 'Massachusetts', 'MA'),
(24, 'Michigan', 'MI'),
(25, 'Minnesota', 'MN'),
(26, 'Mississippi', 'MS'),
(27, 'Missouri', 'MO'),
(28, 'Montana', 'MT'),
(29, 'Nebraska', 'NE'),
(30, 'Nevada', 'NV'),
(31, 'New Hampshire', 'NH'),
(32, 'New Jersey', 'NJ'),
(33, 'New Mexico', 'NM'),
(34, 'New York', 'NY'),
(35, 'North Carolina', 'NC'),
(36, 'North Dakota', 'ND'),
(37, 'Ohio', 'OH'),
(38, 'Oklahoma', 'OK'),
(39, 'Oregon', 'OR'),
(40, 'Pennsylvania', 'PA'),
(41, 'Puerto Rico', 'PR'),
(42, 'Rhode Island', 'RI'),
(43, 'South Carolina', 'SC'),
(44, 'South Dakota', 'SD'),
(45, 'Tennessee', 'TN'),
(46, 'Texas', 'TX'),
(47, 'Utah', 'UT'),
(48, 'Vermont', 'VT'),
(49, 'Virgin Islands', 'VI'),
(50, 'Virginia', 'VA'),
(51, 'Washington', 'WA'),
(52, 'West Virginia', 'WV'),
(53, 'Wisconsin', 'WI'),
(54, 'Wyoming', 'WY');

-- --------------------------------------------------------

--
-- Table structure for table `userAccounts`
--

CREATE TABLE IF NOT EXISTS `userAccounts` (
  `accountID` int(11) NOT NULL AUTO_INCREMENT,
  `accountTypeID` int(11) NOT NULL,
  `email` varchar(80) NOT NULL,
  `username` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `camperID` int(11) DEFAULT NULL,
  `parentID` int(11) DEFAULT NULL,
  `adminID` int(11) DEFAULT NULL,
  `campID` int(11) DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`accountID`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `campID` (`campID`),
  KEY `accountTypeID` (`accountTypeID`),
  KEY `email` (`email`),
  KEY `camperID` (`camperID`),
  KEY `parentID` (`parentID`),
  KEY `adminID` (`adminID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `userAccounts`
--

INSERT INTO `userAccounts` (`accountID`, `accountTypeID`, `email`, `username`, `password`, `camperID`, `parentID`, `adminID`, `campID`, `creation_date`) VALUES
(1, 2, 'ttutt@vt.edu', 'ttutt', 'fdd61bb46843f8b8ef09b8a0cff140ba', NULL, 1700, NULL, NULL, '2010-05-03 21:26:19'),
(2, 3, 'judas@camptest.com', 'testcamp', 'ab25eb41ddb1e992e0e5d588ec6f99d0', NULL, NULL, NULL, 3006, '2010-05-04 23:54:41'),
(3, 3, 'tim.tutt@gmaill.com', 'testcamp2', 'd51364376d846598fb6b8c4fac31d438', NULL, NULL, NULL, 3007, '2010-05-04 23:57:05'),
(4, 2, 'michaelc@yahoo.com', 'michaelc', '5b7c002463a4e54a3343dcf08d69ef97', NULL, 4, NULL, NULL, '2010-05-06 01:30:15'),
(5, 2, 'tim.tutt@gmail.com', 'timtutt', '161ebd7d45089b3446ee4e0d86dbcf92', NULL, 1702, NULL, NULL, '2010-05-19 17:57:03'),
(6, 2, 'jdoe@yahoo.com', 'jdoe', '161ebd7d45089b3446ee4e0d86dbcf92', NULL, 1703, NULL, NULL, '2010-05-23 08:13:38'),
(7, 2, 'jode@yahoo.com', 'jode', '161ebd7d45089b3446ee4e0d86dbcf92', NULL, 1704, NULL, NULL, '2010-06-06 18:19:09'),
(8, 2, 'linus@johnryding.com', 'jryding', '04d583346b5205aa91d59793482841c2', NULL, 1705, NULL, NULL, '2010-06-22 20:27:18');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addressAssociations`
--
ALTER TABLE `addressAssociations`
  ADD CONSTRAINT `addressAssociations_ibfk_1` FOREIGN KEY (`camperID`) REFERENCES `campers` (`camperID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `addressAssociations_ibfk_2` FOREIGN KEY (`parentID`) REFERENCES `parents` (`parentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `addressDetails`
--
ALTER TABLE `addressDetails`
  ADD CONSTRAINT `addressDetails_ibfk_1` FOREIGN KEY (`stateID`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `campAdministratorRelationships`
--
ALTER TABLE `campAdministratorRelationships`
  ADD CONSTRAINT `campAdministratorRelationships_ibfk_1` FOREIGN KEY (`campID`) REFERENCES `camps` (`campID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campAdministratorRelationships_ibfk_2` FOREIGN KEY (`administratorID`) REFERENCES `administrators` (`adminID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `camperInformation`
--
ALTER TABLE `camperInformation`
  ADD CONSTRAINT `camperInformation_ibfk_1` FOREIGN KEY (`camperID`) REFERENCES `camps` (`campID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `camperInformation_ibfk_2` FOREIGN KEY (`camperBirthMonthID`) REFERENCES `months` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `campPrograms`
--
ALTER TABLE `campPrograms`
  ADD CONSTRAINT `campPrograms_ibfk_1` FOREIGN KEY (`ageGroupID`) REFERENCES `ageGroups` (`ageGroupID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `campPrograms_ibfk_2` FOREIGN KEY (`campID`) REFERENCES `camps` (`campID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `camps`
--
ALTER TABLE `camps`
  ADD CONSTRAINT `camps_ibfk_1` FOREIGN KEY (`campStateID`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `demographicInformation`
--
ALTER TABLE `demographicInformation`
  ADD CONSTRAINT `demographicInformation_ibfk_1` FOREIGN KEY (`camperID`) REFERENCES `campers` (`camperID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `demographicInformation_ibfk_2` FOREIGN KEY (`ageGroupID`) REFERENCES `ageGroups` (`ageGroupID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `demographicInformation_ibfk_3` FOREIGN KEY (`ethnicityID`) REFERENCES `ethnicities` (`ethnicityID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `demographicInformation_ibfk_4` FOREIGN KEY (`raceID`) REFERENCES `demographicInformation` (`raceID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parentCamperRelationships`
--
ALTER TABLE `parentCamperRelationships`
  ADD CONSTRAINT `parentCamperRelationships_ibfk_1` FOREIGN KEY (`camperID`) REFERENCES `campers` (`camperID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parentCamperRelationships_ibfk_2` FOREIGN KEY (`parentID`) REFERENCES `parents` (`parentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parentInformation`
--
ALTER TABLE `parentInformation`
  ADD CONSTRAINT `parentInformation_ibfk_1` FOREIGN KEY (`parentID`) REFERENCES `parents` (`parentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `registrations`
--
ALTER TABLE `registrations`
  ADD CONSTRAINT `registrations_ibfk_1` FOREIGN KEY (`campID`) REFERENCES `camps` (`campID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `registrations_ibfk_2` FOREIGN KEY (`camperID`) REFERENCES `campers` (`camperID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `userAccounts`
--
ALTER TABLE `userAccounts`
  ADD CONSTRAINT `userAccounts_ibfk_1` FOREIGN KEY (`accountTypeID`) REFERENCES `accountTypes` (`accountTypeID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `userAccounts_ibfk_2` FOREIGN KEY (`camperID`) REFERENCES `campers` (`camperID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userAccounts_ibfk_3` FOREIGN KEY (`parentID`) REFERENCES `parents` (`parentID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userAccounts_ibfk_4` FOREIGN KEY (`campID`) REFERENCES `camps` (`campID`) ON DELETE CASCADE ON UPDATE CASCADE;
